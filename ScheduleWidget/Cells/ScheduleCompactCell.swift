//
//  ScheduleCompactCell.swift
//  ScheduleWidget
//
//  Created by Генрих Берайлик on 17.05.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import UIKit
import SnapKit

final class ScheduleCompactCell: UITableViewCell {
    
    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupCellViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        timeLabel.text = nil
        nameLabel.text = nil
        roomLabel.text = nil
    }
    
    // MARK: - Interactions
    
    func updateContent(scheduleItem: ScheduleItemModel) {
        nameLabel.text = scheduleItem.className
        roomLabel.text = scheduleItem.roomName
        
        if let timetable = Timetable.getBy(order: scheduleItem.timeCode) {
            timeLabel.text = "\(timetable.startTime)\n\(timetable.endTime)"
        }
    }
    
    // MARK: - Setup
    
    private func setupCellViews() {
        addSubview(timeLabel)
        addSubview(nameLabel)
        addSubview(roomLabel)
        
        timeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(4)
            make.bottom.equalTo(-4)
            make.leading.equalTo(8)
            make.width.equalTo(40)
        }
        
        roomLabel.snp.makeConstraints { (make) in
            make.top.equalTo(4)
            make.bottom.equalTo(-4)
            make.trailing.equalTo(-8)
            make.width.equalTo(40)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(0)
            make.leading.equalTo(timeLabel.snp.trailing).offset(16)
            make.trailing.equalTo(roomLabel.snp.leading).offset(-8)
        }
    }
    
    // MARK: - UI Elements
    
    private let timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 11)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .right
        label.numberOfLines = 2
        return label
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 2
        return label
    }()
    
    private let roomLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
}
