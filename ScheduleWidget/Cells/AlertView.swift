//
//  AlertCell.swift
//  ScheduleWidget
//
//  Created by Генрих Берайлик on 12.07.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import UIKit

final class AlertView: UIView {
    
    // MARK: - Initialization
    
    init(imageName: String?, message: String?) {
        super.init(frame: .zero)
        
        setupViews()
        updateContent(imageName: imageName, message: message)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Interactions
    
    func updateContent(imageName: String?, message: String?) {
        messageLabel.text = message
        if let imageName = imageName {
            iconView.image = UIImage(named: imageName)
        }
    }
    
    // MARK: - Setup
    
    private func setupViews() {
        addSubview(iconView)
        addSubview(messageLabel)
        
        iconView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.snp.centerY)
            make.width.height.equalTo(42)
        }
        
        messageLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.centerY)
            make.centerX.equalToSuperview()
        }
    }
    
    // MARK: - UI Elements
    
    private let iconView: UIImageView = {
        let view = UIImageView()
        
        return view
    }()
    
    private let messageLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 2
        return label
    }()

}
