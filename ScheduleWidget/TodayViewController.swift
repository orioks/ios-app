//
//  TodayViewController.swift
//  ScheduleWidget
//
//  Created by Генрих Берайлик on 17.05.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import UIKit
import NotificationCenter

enum WidgetState {
    case initial
    case error(message: String)
    case data(schedule: [ScheduleItemModel])
}

class TodayViewController: UIViewController, NCWidgetProviding {
        
    // MARK: - Properties
    
    private var scheduleData: [ScheduleItemModel] = []
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        
        tableView.register(ScheduleCompactCell.self, forCellReuseIdentifier: ScheduleCompactCell.cellId)
        tableView.tableFooterView = UIView()
    }
        
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        if let scheduleData = loadSchedule() {
            self.scheduleData = scheduleData
            tableView.tableHeaderView = nil
        } else {
            let alert = AlertView(imageName: "alert", message: "Необходимо настроить учебное расписание")
            
            alert.frame = CGRect(origin: .zero, size: view.frame.size)
            tableView.tableHeaderView = alert
        }
        completionHandler(NCUpdateResult.newData)
        tableView.reloadData()
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        let expanded = activeDisplayMode == .expanded
        preferredContentSize = expanded ? CGSize(width: maxSize.width, height: tableView.contentSize.height) : maxSize
        
        tableView.tableHeaderView?.frame = CGRect(origin: .zero, size: preferredContentSize)
    }
    
    // MARK: - Interactions
    
    private func loadSchedule() -> [ScheduleItemModel]? {
        guard
            let data: Data = GroupCache.fetch(.scheduleData),
            let schedule = try? JSONDecoder().decode([ScheduleItemModel].self, from: data)
            else {
                return nil
        }

        let currentDay = getCurrentWeekDay()
        let todayClasses = schedule.filter({ (item) -> Bool in
            return item.day == currentDay.day.rawValue && item.dayNumber == currentDay.week.rawValue
        })
        
        return todayClasses.sorted(by: { $0.timeCode < $1.timeCode })
    }
    
    private func getCurrentWeekDay() -> (week: WeekType, day: WeekDay) {
        guard let semStart: Date = GroupCache.fetch(.semesterStart) else {
            return (.firstNumerator, .monday)
        }
        let currentDate = Date()
        
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: semStart)
        let date2 = calendar.startOfDay(for: currentDate)

        let components = calendar.dateComponents([.day], from: date1, to: date2)
        let weekCode = ((components.day ?? 0) / 7) % 4
        
        var weekDayCode = calendar.component(.weekday, from: currentDate)
        weekDayCode = (weekDayCode - 1) == 0 ? 7 : (weekDayCode - 1)
        
        let weekType = WeekType(rawValue: weekCode)
        let weekDay = WeekDay(rawValue: weekDayCode)
        
        return (weekType ?? .firstNumerator, weekDay ?? .monday)
    }
    
}

// MARK: - UITableView DataSource

extension TodayViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scheduleData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleCompactCell.cellId, for: indexPath)
        
        if let cell = cell as? ScheduleCompactCell {
            cell.updateContent(scheduleItem: scheduleData[indexPath.row])
        }
        return cell
    }
}

// MARK: - UITableView Delegate

extension TodayViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
