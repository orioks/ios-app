//
//  StudentEventsVC.swift
//  Orioks
//
//  Created by Генрих Берайлик on 30.12.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

final class StudentEventsVC: UITableViewController {
    
    private var disciplineId: Int?
    private var groupId: Int?
    private var studentId: Int?
    private var events: [EventItem] = []
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }
    
    // MARK: - ViewController Configuration
    
    private func setupTableView() {
        tableView.register(EventCell.self, forCellReuseIdentifier: EventCell.cellId)
        tableView.tableFooterView = UIView(frame: .zero)
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshControlChanged), for: .valueChanged)
    }
    
    @objc private func refreshControlChanged() {
        loadStudentEvents()
    }
    
    // MARK: - Interactions
    
    func setData(studentId: Int, disciplineId: Int, groupId: Int) {
        self.disciplineId = disciplineId
        self.groupId = groupId
        self.studentId = studentId
        
        loadStudentEvents()
    }
    
    private func loadStudentEvents() {
        guard let disciplineId = disciplineId, let groupId = groupId, let studentId = studentId else {
            return
        }
        tableView.showLoader()
        DataLoader().getStudentStudyEvents(disciplineId, groupId: groupId, studentId: studentId) { (result) in
            self.events = result
            DispatchQueue.main.async {
                self.tableView.hideLoader()
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    private func onEditEvent(_ event: EventItem, at indexPath: IndexPath) {
        let alertMessage = "Вы можете изменить оценку по мероприятию."
        let alert = UIAlertController(title: event.name, message: alertMessage, preferredStyle: .alert)

        alert.addTextField { (tField) in
            tField.delegate = self
            tField.text = event.currentScore?.description
            tField.placeholder = "12.5"
        }

        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: { [weak self] _ in
            guard let tField = alert.textFields?.first, let text = tField.text else { return }
            self?.updateStudentScore(event, rawScore: text)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func updateStudentScore(_ event: EventItem, rawScore: String) {
        guard let disciplineId = disciplineId, let groupId = groupId, let studentId = studentId, let eventId = event.id else {
            return
        }
        var newScore: Double? = nil
        
        if let doubleValue = Double(rawScore), doubleValue <= event.maxScore {
            newScore = doubleValue
        } else if rawScore == "н" {
            newScore = -1
        }
        
        if event.currentScore == newScore {
            newScore = nil
        }
        
        guard let newScoreValue = newScore else { return }
        DataLoader().updateStudentEventScore(studentId: studentId, disciplineId: disciplineId, groupId: groupId, eventId: eventId, newScore: newScoreValue) { [weak self] (result) in
            self?.handleScoreEvent(result: result)
        }
    }
    
    private func handleScoreEvent(result: EventScoreItem?) {
        if result?.score != nil {
            loadStudentEvents()
        } else {
            let alert = UIAlertController(title: "Ошибка изменения",
                                          message: "Не удалось изменить количество баллов по мероприятию",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - TableView DataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventCell.cellId, for: indexPath) as! EventCell
        
        let event = events[indexPath.row]
        cell.updateContent(week: event.week,
                           name: event.alias ?? event.name,
                           type: event.type,
                           currentScore: event.currentScore,
                           maxScore: event.maxScore)
        return cell
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onEditEvent(events[indexPath.row], at: indexPath)
    }
    
}

extension StudentEventsVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let oldText = textField.text as NSString? else { return true }
        let newStr = oldText.replacingCharacters(in: range, with: string)
        
        print("new value:", newStr)
        if let newValue = Double(newStr) {
            return newValue >= 0
        } else {
            return newStr == "н" || newStr == ""
        }
    }
}
