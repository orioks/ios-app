//
//  TeacherDisciplineCell.swift
//  Orioks
//
//  Created by Генрих Берайлик on 17.12.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

final class TeacherDisciplineCell: UITableViewCell {
    
    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Interactions
    
    func setContent(title: String, subtitle: String?, attribute: String?) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
        attributeLabel.text = attribute
    }
    
    // MARK: - Configure UI
    
    private func setupViews() {
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(attributeLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().inset(UIEdgeInsets(top: 8, left: 16, bottom: 0, right: 0))
            make.trailing.equalTo(attributeLabel.snp.leading).offset(-16)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(2)
            make.leading.bottom.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 16, bottom: 6, right: 0))
            make.trailing.equalTo(attributeLabel.snp.leading).offset(-16)
        }
        
        attributeLabel.snp.makeConstraints { (make) in
            make.trailing.top.bottom.equalToSuperview().inset(UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 16))
            make.width.lessThanOrEqualTo(80)
        }
    }
    
    // MARK: - UI Elements
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        return label
    }()
    
    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .darkGray
        return label
    }()
    
    private let attributeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.layer.cornerRadius = 8
        label.textColor = UIColor.darkGray
        return label
    }()
    
}
