//
//  DisciplineStudentsVC.swift
//  Orioks
//
//  Created by Генрих Берайлик on 29.12.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

final class DisciplineStudentsVC: UITableViewController {
    
    private var disciplineId: Int?
    private var groupId: Int?
    private var students: [StudentStudyResultItem] = []
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    // MARK: - ViewController Configuration
    
    private func setupTableView() {
        tableView.register(DisciplineCell.self, forCellReuseIdentifier: DisciplineCell.cellId)
        tableView.tableFooterView = UIView(frame: .zero)
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshControlChanged), for: .valueChanged)
    }
    
    // MARK: - Interactions
    
    func setData(group: StudentsGroup) {
        navigationItem.title = group.shortName
        self.disciplineId = group.disciplineId
        self.groupId = group.id
        
        loadStudentList()
    }
    
    @objc private func refreshControlChanged() {
        loadStudentList()
    }
    
    private func loadStudentList() {
        guard let disciplineId = disciplineId, let groupId = groupId else {
            return
        }
        tableView.showLoader()
        DataLoader().getDisciplineStudents(disciplineId, groupId: groupId) { (result) in
            self.students = result
            DispatchQueue.main.async {
                self.tableView.hideLoader()
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    // MARK: - TableView DataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DisciplineCell.cellId, for: indexPath) as! DisciplineCell
        
        let student = students[indexPath.row]
        let currentGrade: String? = {
            guard let currentGrade = student.currentGrade else { return nil }
            let grade: String? = currentGrade < 2 ? nil : "\(currentGrade)"
            return "Текущая оценка: \(grade ?? "-")"
        }()
        cell.updateContent(title: student.fullName, subtitle: currentGrade, maxValue: student.totalScore, currentValue: student.currentScore)
        
        return cell
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let disciplineId = disciplineId, let groupId = groupId else {
            return
        }
        let studentItem = students[indexPath.row]
        
        let eventsVC = StudentEventsVC()
        eventsVC.title = studentItem.fullName
        eventsVC.setData(studentId: studentItem.id, disciplineId: disciplineId, groupId: groupId)
        
        navigationController?.pushViewController(eventsVC, animated: true)
    }
    
}
