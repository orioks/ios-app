//
//  TeacherDisciplines.swift
//  Orioks
//
//  Created by Генрих Берайлик on 17.12.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

final class TeacherDisciplinesVC: UITableViewController {
    
    // MARK: - Properties
    
    private var expandedSections: [Int: Bool] = [:]
    private var disciplines: [TeacherDisciplineItem] = []
    private var allDisciplines: [TeacherDisciplineItem] = []
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewController()
        loadDisciplines()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Cache.fetch(.semesterDidChange) == true {
            Cache.put(value: false, for: .semesterDidChange)
            disciplines = []
            tableView.reloadData()
            loadDisciplines()
        }
    }
    
    // MARK: - Data management
    
    private func setupViewController() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        
        // setup tableView
        tableView.keyboardDismissMode = .onDrag
        tableView.register(TeacherDisciplineCell.self, forCellReuseIdentifier: TeacherDisciplineCell.cellId)
        tableView.tableFooterView = UIView(frame: .zero)
        
        // setup refreshControl
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshControlChanged), for: .valueChanged)
        
        // setup searchBar
        let searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        }
    }
    
    @objc private func refreshControlChanged() {
        loadDisciplines()
    }
    
    private func loadDisciplines() {
        tableView.showLoader()
        DataLoader().getTeacherDisciplines { (disciplineHolders) in
            self.allDisciplines = self.groupDisciplines(disciplineHolders)
            self.disciplines = self.allDisciplines
            DispatchQueue.main.async {
                self.tableView.hideLoader()
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
            }
        }
    }

    private func groupDisciplines(_ disciplinesHolders: [TeacherDisciplineHolder]) -> [TeacherDisciplineItem] {
        // заполнение словаря дисциплин по группам
        var dict: [String: [String: [StudentsGroup]]] = [:]
        for holder in disciplinesHolders {
            for item in holder.disciplines {
                let groups = (dict[item.name]?[item.formControl] ?? [])
                var controlsDict: [String: [StudentsGroup]] = [item.formControl: groups]
                for group in item.studyGroups {
                    // добавление идентификатора дисциплины в группу
                    var mutedGroup = group
                    mutedGroup.disciplineId = item.id
                    
                    // получение массива групп по дисциплине и добавление нового элемента
                    var array = controlsDict[item.formControl] ?? []
                    array.append(mutedGroup)
                            
                    // сохранение нового массива групп с сортировкой
                    controlsDict[item.formControl] = array
                        .sorted(by: { $0.shortName < $1.shortName })
                        .sorted(by: { $0.type < $1.type })
                }
                dict[item.name] = controlsDict
            }
        }
        
        // чтение сгруппированных данных их словаря
        var disciplines: [TeacherDisciplineItem] = []
        dict.forEach { (disciplineName, values) in
            values.forEach { (controlFormKey, groups) in
                let newDisciplineItem = TeacherDisciplineItem(id: 0, name: disciplineName, formControl: controlFormKey, studyGroups: groups)
                disciplines.append(newDisciplineItem)
            }
        }
        return disciplines.sorted(by: { $0.name < $1.name })
    }
    
    // MARK: - TableView DataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return disciplines.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let isExpanded = expandedSections[section] ?? true
        return isExpanded ? disciplines[section].studyGroups.count : 0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let isExpanded = expandedSections[section] ?? true
        let headerView = SectionHeaderView(title: disciplines[section].name,
                                           subtitle: disciplines[section].formControl,
                                           isExpanded: isExpanded)
        headerView.onExpandTap = { didExpand in
            self.expandedSections[section] = didExpand
            tableView.reloadSections([section], with: .automatic)
        }
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TeacherDisciplineCell.cellId) as! TeacherDisciplineCell
        let group = disciplines[indexPath.section].studyGroups[indexPath.row]
        
        if group.type == 1 {
            cell.setContent(title: group.fullName, subtitle: group.group?.fullName, attribute: group.getAttributesText())
        } else {
            let studentsCount: String? = group.numberStudents != nil ? "Количество студентов: \(group.numberStudents ?? 0)" : nil
            cell.setContent(title: group.fullName, subtitle: studentsCount, attribute: group.getAttributesText())
        }
        return cell
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let group = disciplines[indexPath.section].studyGroups[indexPath.row]
        guard let disciplineId = group.disciplineId else {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        if group.type == 1, let groupId = group.group?.id {
            let eventsVC = StudentEventsVC()
            eventsVC.title = group.fullName
            eventsVC.setData(studentId: group.id, disciplineId: disciplineId, groupId: groupId)
            navigationController?.pushViewController(eventsVC, animated: true)
        } else {
            let studentsVC = DisciplineStudentsVC()
            studentsVC.setData(group: group)
            navigationController?.pushViewController(studentsVC, animated: true)
        }
    }
}

// MARK: - UISearchResults Updating

extension TeacherDisciplinesVC: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        defer {
            tableView.reloadData()
        }
        guard let query = searchController.searchBar.text, query.count > 0 else {
            disciplines = allDisciplines
            return
        }
        disciplines = allDisciplines.compactMap({ (disciplineItem) -> TeacherDisciplineItem? in
            let groups = disciplineItem.studyGroups.filter {
                $0.fullName.hasQuerySubstring(query)
            }
            var filteredDiscipline: TeacherDisciplineItem? = TeacherDisciplineItem(
                id: disciplineItem.id,
                name: disciplineItem.name,
                formControl: disciplineItem.formControl,
                studyGroups: groups
            )
        
            if groups.isEmpty && filteredDiscipline?.name.hasQuerySubstring(query) == false {
                filteredDiscipline = nil
            }
            return filteredDiscipline
        })
    }
}
