//
//  AcademicDebtVC.swift
//  Orioks
//
//  Created by Генрих Берайлик on 19/02/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

final class AcademicDebtVC: UITableViewController {
    
    // MARK: - Properties
    
    var debtItem: AcademicDebtItem? {
        didSet {
            fetchEvents()
        }
    }
    
    private var resits: [ResitItem] = [] {
        didSet {
            updateData()
        }
    }
    
    // MARK: - ViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.cellId)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.tableHeaderView = headerView
    }
    
    // MARK: - Fetching data
    
    private func updateData() {
        DispatchQueue.main.async {
            self.tableView.hideLoader()
            self.tableView.reloadData()
        }
    }
    
    private func fetchEvents() {
        guard let debtItem = self.debtItem else { return }
        self.title = debtItem.name
        
        headerView.debtItem = debtItem
        tableView.layoutTableHeaderView()
        
        tableView.showLoader()
        DataLoader().loadAcademicDebtResits(debtId: debtItem.id) { (resitItems) in
            let sortedResits = resitItems?.sorted(by: { $0.resitNumber > $1.resitNumber })
            self.resits = sortedResits ?? []
        }
    }
    
    // MARK: - Helper methods
    
    private func readableDate(_ dateStr: String) -> String {
        let isoDateFormatter = ISO8601DateFormatter()
        if let date = isoDateFormatter.date(from: dateStr) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy, HH:mm"
            return dateFormatter.string(from: date)
        }
        return ""
    }
    
    // MARK: - TableView Data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return debtItem?.consultationTime.count ?? 0
        default:
            return resits.isEmpty ? 1 : resits.count
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Консультации"
        default:
            return "Пересдачи"
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.cellId, for: indexPath)
        
        cell.textLabel?.text = nil
        cell.detailTextLabel?.text = nil
        
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = debtItem?.consultationTime[indexPath.row]
        default:
            if resits.isEmpty {
                cell.textLabel?.text = "Не назначены"
            } else {
                let resit = resits[indexPath.row]
                cell.textLabel?.text = "Дата: \(readableDate(resit.datetime))"
                cell.detailTextLabel?.text = "Аудитория: \(resit.classroom)"
            }
        }
        
        return cell
    }

    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - UI Elements
    
    private var headerView = DebtResitsView()
    
}
