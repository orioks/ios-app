//
//  DisciplinesVC.swift
//  Orioks
//
//  Created by Gena Beraylik on 02.10.2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import UIKit

class DisciplinesVC: UITableViewController {
    
    // MARK: - Properties
    
    var disciplines: [DisciplineItem] = []
    var academicDebts: [AcademicDebtItem] = []
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.largeTitleDisplayMode = .always
        }
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.register(DisciplineCell.self, forCellReuseIdentifier: DisciplineCell.cellId)
        tableView.register(AcademicDebtCell.self, forCellReuseIdentifier: AcademicDebtCell.cellId)
        
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Cache.fetch(.semesterDidChange) == true {
            Cache.put(value: false, for: .semesterDidChange)
            disciplines = []
            academicDebts = []
            tableView.reloadData()
            reloadData()
        }
    }
    
    // MARK: - Interactions
    
    @objc func reloadData() {
        tableView.showLoader()
        DispatchQueue.init(label: "orioks.bg.thread", qos: .background).async { [weak self] in
            guard let aSelf = self else { return }
            let group = DispatchGroup()
            group.enter()
            DataLoader().loadDisciplines() { (result) in
                group.leave()
                guard let result = result else { return }
                aSelf.disciplines = result.sorted(by: { $0.name < $1.name })
            }
            
            group.enter()
            DataLoader().loadAcademicDebts { (debtItems) in
                group.leave()
                if let debtItems = debtItems, !debtItems.isEmpty {
                    aSelf.academicDebts = debtItems
                }
            }
            
            group.wait()
            DispatchQueue.main.async {
                aSelf.tableView.hideLoader()
                aSelf.tableView.reloadData()
            }
        }
        
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let eventsVC = EventsVC()
            eventsVC.discipline = disciplines[indexPath.row]
            navigationController?.pushViewController(eventsVC, animated: true)
        } else if indexPath.section == 1 {
            let academicDebtVC = AcademicDebtVC()
            academicDebtVC.debtItem = academicDebts[indexPath.row]
            navigationController?.pushViewController(academicDebtVC, animated: true)
        }
    }
    
    // MARK: - TableView DataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return academicDebts.isEmpty ? 1 : 2
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? nil : "Долги"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return disciplines.count
        } else if section == 1 {
            return academicDebts.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AcademicDebtCell.cellId, for: indexPath) as! AcademicDebtCell
            
            cell.debt = academicDebts[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: DisciplineCell.cellId, for: indexPath) as! DisciplineCell
            
            cell.discipline = disciplines[indexPath.row]
            return cell
        }
    }
    
}
