//
//  EventsVC.swift
//  Orioks
//
//  Created by Gena Beraylik on 10/10/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import UIKit

final class EventsVC: UITableViewController {
    
    // MARK: - Properties
    
    var discipline: DisciplineItem? {
        didSet {
            fetchEvents()
        }
    }
    
    private var events: [EventItem] = [] {
        didSet {
            updateData()
        }
    }
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
    }
    
    // MARK: - Configurations
    
    private func setupTableView() {
        tableView.register(EventCell.self, forCellReuseIdentifier: EventCell.cellId)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.tableHeaderView = headerView
    }
    
    // MARK: - Interactions
    
    private func updateData() {
        tableView.reloadData()
    }
    
    private func fetchEvents() {
        guard let discipline = self.discipline else { return }
        tableView.showLoader()
        self.title = discipline.name
        
        headerView.discipline = discipline
        tableView.layoutTableHeaderView()
        
        DataLoader().loadEvents(disciplineId: discipline.id) { (eventItems) in
            DispatchQueue.main.async {
                self.tableView.hideLoader()
                self.events = eventItems ?? []
            }
        }
    }
    
    // MARK: - TableView DataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Мероприятия"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventCell.cellId, for: indexPath) as! EventCell
        
        cell.event = events[indexPath.row]
        
        return cell
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // MARK: - UI Elements
    
    private var headerView = DisciplineDetailsView()
}
