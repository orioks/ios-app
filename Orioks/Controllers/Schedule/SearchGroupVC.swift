//
//  SearchGroupVC.swift
//  Orioks
//
//  Created by Генрих Берайлик on 30/09/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

final class SearchGroupVC: UITableViewController {
    
    // MARK: - Properties
    
    private var allGroups: [String] = []
    private var groups: [String] = []
    private var dataLoader: DataLoader?
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        injectDependencies()
        setupTableView()
        setupNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setKeyboardEvents()
        loadGroups()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        searchBar.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        resignKeyboardEvents()
    }
    
    // MARK: - Delegate closures
    
    var didSelectGroup: ((String) -> Void)?
    
    // MARK: - Data management
    
    private func injectDependencies() {
        dataLoader = DataLoader()
    }
    
    private func loadGroups() {
        dataLoader?.getMietGroups(completion: { [weak self] groups in
            DispatchQueue.main.async {
                self?.groups = groups
                self?.allGroups = groups
                self?.tableView.reloadData()
            }
        })
    }
    
    // MARK: - Keyboard management
    
    private func setKeyboardEvents() {
        registerForKeyboardWillHideNotification(tableView) { [weak self] keyboardSize in
            self?.updateContentWithKeyboard(size: keyboardSize)
        }
        registerForKeyboardWillShowNotification(tableView) { [weak self] keyboardSize in
            self?.updateContentWithKeyboard(size: keyboardSize)
        }
    }
    
    private func resignKeyboardEvents() {
        NotificationCenter.default.removeObserver(self)
    }

    func registerForKeyboardWillShowNotification(_ scrollView: UIScrollView, usingBlock block: ((CGSize?) -> Void)? = nil) {
        _ = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil, using: { notification -> Void in
            let userInfo = notification.userInfo!
            let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
            block?(keyboardSize)
        })
    }

    func registerForKeyboardWillHideNotification(_ scrollView: UIScrollView, usingBlock block: ((CGSize?) -> Void)? = nil) {
        _ = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil, using: { notification -> Void in
            let userInfo = notification.userInfo!
            let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
            block?(keyboardSize)
        })
    }
    
    private func updateContentWithKeyboard(size: CGSize?) {
        var contentInsets = tableView.contentInset
        contentInsets.bottom = size?.height ?? 0
        tableView.contentInset = contentInsets
        tableView.scrollIndicatorInsets = contentInsets
    }
    
    // MARK: - Configure UI
    
    private func setupTableView() {
        // Register cell
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.cellId)
    }
    
    private func setupNavBar() {
        navigationItem.titleView = searchBar
    }
    
    // MARK: - TableView DataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.cellId, for: indexPath)
        cell.textLabel?.text = groups[indexPath.row]
        return cell
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectGroup?(groups[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UI Elements
    
    private lazy var searchBar: UISearchBar = {
        let view = UISearchBar()
        view.showsCancelButton = true
        view.searchBarStyle = .prominent
        view.placeholder = "Поиск..."
        view.delegate = self
        return view
    }()
    
}

// MARK: - SearchBar Delegate

extension SearchGroupVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        groups = allGroups.filter({
            $0.hasQuerySubstring(searchText)
        })
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        dismiss(animated: true, completion: nil)
    }
    
}
