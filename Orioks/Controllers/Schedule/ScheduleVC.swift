//
//  ScheduleVC.swift
//  Orioks
//
//  Created by Генрих Берайлик on 17/07/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

class ScheduleVC: UITableViewController {
    
    // MARK: - Properties
    
    private var scheduleFilter: ScheduleFilter = .today
    private var viewModel: ScheduleViewModel?
    private var groupButton: UIBarButtonItem?
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupTableView()
        setupViewModel()
        setupGroupButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel?.loadData()
    }
    
    // MARK: - Interactions
    
    private func setupViewModel() {
        viewModel = ScheduleViewModel()
        viewModel?.didChangeState = { [weak self] in
            self?.updateData()
        }
    }
    
    private func updateData() {
        guard let state = viewModel?.state else { return }
        switch state {
        case .finished:
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        default:
            break
        }
    }
    
    @objc func changeGroupTapped() {
        let groupsVC = SearchGroupVC()
        groupsVC.didSelectGroup = { [weak self] group in
            (self?.tableView.tableHeaderView as? ScheduleHeaderView)?.setSelected(index: 0)
            self?.viewModel?.changeGroup(group)
            self?.setupGroupButton()
        }
        let navVC = UINavigationController(rootViewController: groupsVC)
        present(navVC, animated: true, completion: nil)
    }
    
    // MARK: - Configure UI
    
    private func setupTableView() {
        // Register cell
        tableView.register(ScheduleCell.self, forCellReuseIdentifier: ScheduleCell.cellId)
        
        // Header view
        let headerView = ScheduleHeaderView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 64))
        headerView.delegate = self
        headerView.setSelected(index: 0)
        tableView.tableHeaderView = headerView
    }
    
    private func setupGroupButton() {
        let title: String = viewModel?.selectedGroup ?? "Группа"
        groupButton = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(changeGroupTapped))
        navigationItem.rightBarButtonItem = groupButton
    }
    
    // MARK: - TableView DataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.dataSource.sections.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.dataSource.sections[section].rows.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleCell.cellId, for: indexPath) as! ScheduleCell
        
        guard let dataSource = viewModel?.dataSource else { return cell }
        let item = dataSource.sections[indexPath.section].rows[indexPath.row]
        cell.setViewModel(item)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel?.dataSource.sections[section].title
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

// MARK: - ScheduleHeader Delegate

extension ScheduleVC: ScheduleHeaderDelegate {
    func didSelectScheduleFilter(_ filter: ScheduleFilter) {
        viewModel?.changeScheduleFilter(filter)
    }
}
