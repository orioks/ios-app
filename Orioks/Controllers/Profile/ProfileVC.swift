//
//  ProfileVC.swift
//  Orioks
//
//  Created by Генрих Берайлик on 12/11/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation
import UIKit

class ProfileVC: UITableViewController {

    private var presenter: ProfilePresenter?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupPresenter()
        setupViews()
        prepareData()
    }
    
    // MARK: - Interactions
    
    func showLoader() {
        tableView.showLoader()
    }
    
    func hideLoader() {
        tableView.hideLoader()
    }
    
    // MARK: - Data management
    
    private func setupPresenter() {
        presenter = ProfilePresenter()
        presenter?.view = self
        presenter?.profileDidUpdate = { [weak self] in
            self?.updateTableData()
        }
    }
    
    private func prepareData() {
        showLoader()
        self.presenter?.fetchProfile()
    }
    
    private func updateTableData() {
        DispatchQueue.main.async {
            self.hideLoader()
            self.tableView.reloadData()
            self.headerView.profile = self.presenter?.profile
        }
    }
    
    // MARK: - ActionSheet interactions
    
    @objc private func openEditMenu() {
        pickerView.dissmiss()
        let changeSemesterAction = UIAlertAction(title: "Изменить семестр", style: .default) { [weak self] (_) in
            self?.showSemesterPicker()
        }
        let changeRoleAction = UIAlertAction(title: "Изменить профиль", style: .default) { [weak self] (_) in
            self?.showRolePicker()
        }
        let logoutAction = UIAlertAction(title: "Выход", style: .destructive) { [weak self] (_) in
            self?.logout()
        }
        
        let actionSheet = UIAlertController(title: "Настройки", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(changeSemesterAction)
        actionSheet.addAction(changeRoleAction)
        actionSheet.addAction(logoutAction)
        actionSheet.addAction(UIAlertAction(title: "Отменить", style: .cancel))
        
        present(actionSheet, animated: true)
    }
    
    private func logout() {
        presenter?.logout()
    }
    
    private func showSemesterPicker() {
        guard let semesters = presenter?.semestersInfo else { return }
        let titles = semesters.list.map({ $0.name })
        let selected = semesters.list.firstIndex(where: { $0.id == semesters.current })
        pickerView.show(with: titles, selectedIndex: selected) { [weak self] index in
            self?.presenter?.changeSemester(index)
        }
    }
    
    private func showRolePicker() {
        guard let roles = presenter?.rolesInfo else { return }
        let titles = roles.list.compactMap({ $0.description })
        let selected = roles.list.firstIndex(where: { $0.role == roles.current })
        pickerView.show(with: titles, selectedIndex: selected) { [weak self] index in
            self?.presenter?.changeRole(index)
        }
    }
    
    // MARK: - Configure UI
    
    private func setupViews() {
        setupNavBar()
        setupTableView()
        setupPickerView()
    }
    
    private func setupNavBar() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.largeTitleDisplayMode = .always
        }
        let editBtn = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(openEditMenu))
        navigationItem.rightBarButtonItem = editBtn
    }
    
    private func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.cellId)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.tableHeaderView = headerView
        
        headerView.frame.size = CGSize(width: view.frame.width, height: view.frame.width * 9 / 16)
    }
    
    private func setupPickerView() {
        view.addSubview(pickerView)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return presenter?.tableData.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return presenter?.tableData[section].section
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.cellId, for: indexPath)
        
        cell.textLabel?.text = presenter?.tableData[indexPath.section].row
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byTruncatingTail
        
        return cell
    }
    
    // MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // MARK: - UI Elements
    
    private let headerView = ProfileHeaderView()
    private let pickerView = AlphaPickerView()
}
