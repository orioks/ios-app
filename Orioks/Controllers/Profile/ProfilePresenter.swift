//
//  ProfilePresenter.swift
//  Orioks
//
//  Created by Генрих Берайлик on 30.01.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import Foundation

class ProfilePresenter {
    
    // MARK: - Properties
    weak var view: ProfileVC?
    var semestersInfo: SemestersHolder?
    var rolesInfo: UserRolesHolder?
    var profile: ProfileItem?
    
    var tableData: [TableRowModel] = [] {
        didSet {
            profileDidUpdate?()
        }
    }
    
    // MARK: - Callbacks
    
    var profileDidUpdate: (() -> Void)?
    
    // MARK: - Interactions
    
    func fetchProfile() {
        guard let roleName: String = Cache.fetch(.userRole), let role = UserRole(role: roleName) else {
            return
        }
        DispatchQueue.init(label: "orioks.bg.thread", qos: .background).async { [weak self] in
            let group = DispatchGroup()
            group.enter()
            switch role {
            case .student:
                DataLoader().loadProfile { (profile) in
                    self?.profile = profile
                    group.leave()
                }
            case .teacher:
                DataLoader().loadTeacherProfile { (profile) in
                    self?.profile = profile
                    group.leave()
                }
            }
            
            group.enter()
            ApiService.shared.getRoles { [weak self] (roles) in
                self?.rolesInfo = roles
                group.leave()
            }
            
            group.enter()
            ApiService.shared.getSemesters { [weak self] (semesters) in
                self?.semestersInfo = semesters
                group.leave()
            }
            
            group.wait()
            self?.prepareTableData()
        }
    }
    
    func changeRole(_ newIndex: Int) {
        guard let roles = rolesInfo?.list else { return }
        let newRole = roles[newIndex]
        if newRole.role == rolesInfo?.current {
            return
        }
        view?.showLoader()
        DataLoader().changeRole(newRole: newRole.role) { (success) in
            DispatchQueue.main.async {
                if success {
                    Cache.put(value: newRole.role, for: .userRole)
                    AppDelegate.shared?.coordinator.start()
                } else {
                    self.view?.hideLoader()
                }
            }
        }
        print(newRole)
    }
    
    func changeSemester(_ newIndex: Int) {
        guard let semesters = semestersInfo?.list else { return }
        let newSemester = semesters[newIndex]
        if newSemester.id == semestersInfo?.current {
            return
        }
        view?.showLoader()
        DataLoader().changeSemester(newSemester: newSemester.id) { (success) in
            if success {
                Cache.put(value: true, for: .semesterDidChange)
                self.semestersInfo?.current = newSemester.id
                self.prepareTableData()
            } else {
                DispatchQueue.main.async {
                    self.view?.hideLoader()
                }
            }
        }
    }
    
    func logout() {
        PushService.unregisterPush()
        CoreDataService.clearDataBase()
        DispatchQueue.main.async {
            Cache.clear()
            GroupCache.clear()
            AppDelegate.shared?.coordinator.start()
        }
        ApiService.shared.deleteAuthToken { (successed) in
            print("Did logout: \(successed)")
        }
    }
    
    // MARK: - Data management
    
    private func prepareTableData() {
        guard let roleName: String = Cache.fetch(.userRole),
            let role = UserRole(role: roleName),
            let profile = profile else {
                return
        }
        switch role {
        case .student:
            tableData = prepareStudentData(profile)
        case .teacher:
            tableData = prepareTeacherData(profile)
        }
    }
    
    private func prepareStudentData(_ profile: ProfileItem) -> [TableRowModel] {
        let currentSemester = semestersInfo?.list.first(where: {
            $0.id == semestersInfo?.current
        })
        let data: [TableRowModel] = [
            TableRowModel(section: "Семестр", row: currentSemester?.name),
            TableRowModel(section: "Профиль", row: profile.role?.name),
            TableRowModel(section: "Институт", row: profile.institute?.fullName),
            TableRowModel(section: "Направление", row: profile.studyDirection),
            TableRowModel(section: "Профиль обучения", row: profile.studyProfile)
        ]
        return data
    }
    
    private func prepareTeacherData(_ profile: ProfileItem) -> [TableRowModel] {
        let currentSemester = semestersInfo?.list.first(where: {
            $0.id == semestersInfo?.current
        })
        let data: [TableRowModel] = [
            TableRowModel(section: "Семестр", row: currentSemester?.name),
            TableRowModel(section: "Профиль", row: profile.role?.name),
            TableRowModel(section: "Институт", row: profile.institute?.fullName)
        ]
        return data
    }
    
}

struct TableRowModel {
    var section: String?
    var row: String?
}
