//
//  ViewController.swift
//  Orioks
//
//  Created by Gena Beraylik on 02.10.2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import UIKit

class AuthenticationVC: UIViewController {

    // MARK: - ViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .lightGray
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
        
        // Keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setupBackground()
        
        addSubviews()
        setupLoginCard()
        setupConstraints()
        configureAppearance()
        
    }
    
    // MARK: - Selectors
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    @objc func loginUser() {
        errorLabel.text = ""
        guard let login = loginInput.textField.text, login.count != 0 else {
            errorLabel.text = "Введите ваш логин"
            return
        }
        guard let password = passwordInput.textField.text, password.count != 0 else {
            errorLabel.text = "Введите ваш пароль"
            return
        }
        
        errorLabel.text = ""
        loginButton.setLoading(true)
        
        ApiService.shared.authRequest(login: login, password: password) {data in            DispatchQueue.main.async {
                self.loginButton.setLoading(false)
            }
            if let error = data["error"] as? String {
                DispatchQueue.main.async {
                    self.errorLabel.text = error
                    return
                }
            }
            guard let token = data["token"] as? String else { return }
            if let userRole = data["currentRole"] as? String {
                Cache.put(value: token, for: .token)
                Cache.put(value: userRole, for: .userRole)
                self.gotoDisciplines()
            } else {
                ApiService.shared.deleteAuthToken { (successed) in
                    Cache.clear()
                }
                DispatchQueue.main.async {
                    self.errorLabel.text = "У вашей учетной записи не настроен доступ к системе ОРИОКС. \nОбратитесь к администратору."
                    return
                }
            }
            
        }
    }
    
    func gotoDisciplines() {
        DispatchQueue.main.async {
            AppDelegate.shared?.coordinator.start()
        }
    }
    
    @objc func handleKeyboard(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            guard let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
                return
            }
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            let cardViewBottom = loginCardView.frame.origin.y + loginCardView.frame.height
            var yOffset = keyboardFrame.height - (view.frame.height - cardViewBottom)
            
            yOffset = yOffset < 0 ? 0 : yOffset
            
            self.view.frame.origin.y = isKeyboardShowing ? -yOffset : 0
        }
    }
    
    // MARK: - Configure appearance
    
    func addSubviews() {
        view.addSubview(orioksLabel)
        view.addSubview(orioksLogo)
        view.addSubview(loginCardView)
        
        orioksLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        orioksLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 32).isActive = true
        orioksLabel.bottomAnchor.constraint(equalTo: orioksLogo.topAnchor).isActive = true
    }
    
    func setupLoginCard() {
        loginCardView.addSubview(loginInput)
        loginCardView.addSubview(passwordInput)
        loginCardView.addSubview(loginButton)
        loginCardView.addSubview(errorLabel)
        
        addCardConstraints()
    }
    
    func setupConstraints() {
        let cardRatio: CGFloat = 320 / self.view.frame.height
        
        orioksLogo.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        orioksLogo.centerYAnchor.constraint(equalTo: loginCardView.topAnchor).isActive = true
        orioksLogo.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.25).isActive = true
        orioksLogo.heightAnchor.constraint(equalTo: orioksLogo.widthAnchor).isActive = true
        
        loginCardView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginCardView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 48).isActive = true
        loginCardView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: cardRatio).isActive = true
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            loginCardView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        } else {
            loginCardView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
        }
    }
    
    func addCardConstraints() {
        loginInput.topAnchor.constraint(equalTo: orioksLogo.bottomAnchor, constant: 16).isActive = true
        loginInput.centerXAnchor.constraint(equalTo: loginCardView.centerXAnchor).isActive = true
        loginInput.widthAnchor.constraint(equalTo: loginCardView.widthAnchor, multiplier: 0.8).isActive = true
        
        passwordInput.topAnchor.constraint(equalTo: loginInput.bottomAnchor, constant: 16).isActive = true
        passwordInput.centerXAnchor.constraint(equalTo: loginCardView.centerXAnchor).isActive = true
        passwordInput.widthAnchor.constraint(equalTo: loginCardView.widthAnchor, multiplier: 0.8).isActive = true
        passwordInput.heightAnchor.constraint(equalTo: loginInput.heightAnchor).isActive = true
        
        errorLabel.topAnchor.constraint(equalTo: passwordInput.bottomAnchor, constant: 8).isActive = true
        errorLabel.centerXAnchor.constraint(equalTo: loginCardView.centerXAnchor).isActive = true
        errorLabel.bottomAnchor.constraint(equalTo: loginButton.topAnchor).isActive = true
        errorLabel.widthAnchor.constraint(equalTo: loginCardView.widthAnchor, multiplier: 0.8).isActive = true
        
        loginButton.centerXAnchor.constraint(equalTo: loginCardView.centerXAnchor).isActive = true
        loginButton.widthAnchor.constraint(equalTo: loginCardView.widthAnchor, multiplier: 0.8).isActive = true
        loginButton.heightAnchor.constraint(equalTo: loginInput.heightAnchor).isActive = true
        loginButton.bottomAnchor.constraint(equalTo: loginCardView.bottomAnchor, constant: -16).isActive = true
    }
    
    func setupBackground() {
        let backgroundView = UIImageView()
        backgroundView.image = UIImage.init(named: "background")
        backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        backgroundView.contentMode = .scaleAspectFill
        backgroundView.frame = view.bounds
        view.addSubview(backgroundView)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
    }
    
    func configureAppearance() {
        orioksLabel.text = "ОРИОКС"
        orioksLogo.image = UIImage(named: "logomiet")
        
        loginInput.textField.autocapitalizationType = .none
        loginInput.textField.autocorrectionType = .no
        loginInput.fieldIcon.image = UIImage.init(named: "man")
        loginInput.textField.attributedPlaceholder = NSAttributedString(string: "Логин", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        passwordInput.fieldIcon.image = UIImage.init(named: "locked")
        passwordInput.textField.isSecureTextEntry = true
        passwordInput.textField.attributedPlaceholder = NSAttributedString(string: "Пароль", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        loginButton.setTitle("Войти", for: .normal)
        
        errorLabel.textColor = .red
    }
    
    
    // MARK: - UI Elements
    
    let orioksLogo: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.zPosition = 1
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 8
        view.layer.shadowOpacity = 0.3
        return view
    }()
    
    let loginInput: InputFieldView = {
        let view = InputFieldView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let passwordInput: InputFieldView = {
        let view = InputFieldView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let loginButton: LoginButton = {
        let view = LoginButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(loginUser), for: .touchUpInside)
        return view
    }()
    
    let loginCardView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 24
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 16
        view.layer.shadowOpacity = 0.3
        return view
    }()
    
    let errorLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byTruncatingTail
        return view
    }()
    
    let orioksLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.systemFont(ofSize: 56)
        view.textColor = UIColor.init(red: 0.6, green: 0.7843, blue: 0.8392, alpha: 1)
        return view
    }()
    
}

