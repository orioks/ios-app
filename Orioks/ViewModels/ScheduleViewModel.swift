//
//  ScheduleViewModel.swift
//  Orioks
//
//  Created by Генрих Берайлик on 05/08/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

enum ScheduleFilter: String, CaseIterable {
    case today = "Сегодня"
    case tomorrow = "Завтра"
    case currentWeek = "Неделя"
    case nextWeek = "Следующая неделя"
    case month = "Месяц"
}

struct ScheduleItemViewModel {
    var subjectTitle: String
    var subjectDescription: String
    var timing: String?
    
    init(model: ScheduleItemModel) {
        subjectTitle = model.className
        subjectDescription = "🚪\(model.roomName)  👤\(model.classTeacher)"
        if let timetable = Timetable.getBy(order: model.timeCode) {
            timing = "\(timetable.startTime)\n\(timetable.endTime)"
        }
    }
    
}

enum ViewModelState {
    case idle
    case loading
    case finished
    case error(String?)
}

class ScheduleViewModel {
    
    // MARK: - Properties
    
    private(set) var dataSource: ScheduleDataSource = ScheduleDataSource()
    
    private var scheduleModel: [ScheduleItemModel] = []
    private let dataLoader: DataLoader
    private let service: ScheduleService
    
    private(set) var selectedGroup: String? {
        get {
            return UserDefaults.standard.string(forKey: "SelectedGroup")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "SelectedGroup")
        }
    }
    
    private(set) var state: ViewModelState = .idle {
        didSet {
            didChangeState?()
        }
    }
    
    // MARK: - Initialization
    
    init() {
        dataLoader = DataLoader()
        service = ScheduleService()
    }
    
    // MARK: - Closures
    
    var didChangeState: (() -> Void)?
    
    // MARK: - Interactions
    
    func loadData() {
        guard let selectedGroup = selectedGroup else {
            state = .error("Failed load group name")
            return
        }
        state = .loading
        dataLoader.getSchedule(groupName: selectedGroup) { [weak self] (result) in
            guard let resultData = result?.data else {
                self?.state = .error("Cannot load schedule")
                return
            }
            self?.saveSchedule(resultData)
            self?.scheduleModel = resultData
            self?.changeScheduleFilter(.today)
        }
    }
    
    func changeGroup(_ group: String) {
        selectedGroup = group
        loadData()
    }
    
    func changeScheduleFilter(_ filter: ScheduleFilter) {
        state = .loading
        
        guard !self.scheduleModel.isEmpty else {
            state = .error("Schedule not loaded")
            return
        }
        var filteredClasses: [ScheduleItemModel] = []
        let currentDay = service.getCurrentWeekDay()
        
        switch filter {
        case .today:
            filteredClasses = scheduleModel.filter({ (item) -> Bool in
                return item.day == currentDay.day.rawValue && item.dayNumber == currentDay.week.rawValue
            })
        case .tomorrow:
            filteredClasses = scheduleModel.filter({ (item) -> Bool in
                return item.day == currentDay.day.rawValue+1 && item.dayNumber == currentDay.week.rawValue
            })
        case .currentWeek:
            filteredClasses = scheduleModel.filter({ (item) -> Bool in
                return item.dayNumber == currentDay.week.rawValue
            })
        case .nextWeek:
            filteredClasses = scheduleModel.filter({ (item) -> Bool in
                return item.dayNumber == currentDay.week.rawValue+1
            })
        case .month:
            filteredClasses = scheduleModel
        }
        setupDataSource(items: filteredClasses)
    }
    
    private func setupDataSource(items: [ScheduleItemModel]) {
        var output: [ScheduleSectionItem] = []
        
        // map schedule to week types
        let scheduleDict = Dictionary(grouping: items, by: { WeekType(rawValue: $0.dayNumber) })
        let weekDictKeys = scheduleDict.keys.compactMap({ $0 }).sorted(by: { $0.rawValue < $1.rawValue })
        for weekDictKey in weekDictKeys {
            
            // add section with week name to output
            output.append(ScheduleSectionItem(title: weekDictKey.toString(), rows: []))
            
            // map week schedule to week days
            
            let weekDaysDict = Dictionary(grouping: scheduleDict[weekDictKey]!, by: { WeekDay(rawValue: $0.day) })
            let daysDictKeys = weekDaysDict.keys.compactMap({ $0 }).sorted(by: { $0.rawValue < $1.rawValue })
            for daysDictKey in daysDictKeys {
                
                // sort and convert day lessons to view model list
                let convertedList = weekDaysDict[daysDictKey]!
                    .sorted(by: { $0.timeCode < $1.timeCode })
                    .map({ ScheduleItemViewModel(model: $0) })
                
                // add section with day name and lessons to output
                output.append(ScheduleSectionItem(title: daysDictKey.toString(), rows: convertedList))
            }
        }
        dataSource.sections = output
        state = .finished
    }
 
    // MARK: - Storage
    
    private func saveSchedule(_ data: [ScheduleItemModel]) {
        let dataEncoded = try? JSONEncoder().encode(data)
        
        GroupCache.put(value: dataEncoded, for: .scheduleData)
    }
    
}

