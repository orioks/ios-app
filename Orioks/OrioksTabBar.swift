//
//  OrioksTabBar.swift
//  Orioks
//
//  Created by Генрих Берайлик on 12/11/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation
import UIKit

class OrioksTabBar: UITabBarController {
    
    private let role: UserRole
    
    // MARK: - Initialization
    
    init(role: UserRole) {
        self.role = role
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBars()
    }
    
    // MARK: - Data management
    
    private func setupTabBars() {
        switch role {
        case .student:
            PushService.requestPushNotifications()
            setStudentTabBars()
        case .teacher:
            setTeachersTabBars()
        }
    }
    
    private func setStudentTabBars() {
        viewControllers = [
            makeTabBar(title: "Дисциплины", controller: DisciplinesVC(), tabIcon: UIImage(named: "diary"), tag: 0),
            makeTabBar(title: "Расписание", controller: ScheduleVC(), tabIcon: UIImage(named: "calendar"), tag: 1),
            makeTabBar(title: "Профиль", controller: ProfileVC(), tabIcon: UIImage(named: "menu"), tag: 2)
        ]
    }
    
    private func setTeachersTabBars() {
        viewControllers = [
            makeTabBar(title: "Дисциплины", controller: TeacherDisciplinesVC(), tabIcon: UIImage(named: "diary"), tag: 0),
            makeTabBar(title: "Профиль", controller: ProfileVC(), tabIcon: UIImage(named: "menu"), tag: 1)
        ]
    }
    
    private func makeTabBar(title: String, controller: UIViewController, tabIcon: UIImage?, tag: Int) -> UIViewController {
        controller.title = title
        let navVC = UINavigationController(rootViewController: controller)
        navVC.tabBarItem = UITabBarItem(title: title, image: tabIcon, tag: tag)
        return navVC
    }
    
}
