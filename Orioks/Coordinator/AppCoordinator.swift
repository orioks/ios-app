//
//  MainCoordinator.swift
//  Orioks
//
//  Created by Генрих Берайлик on 27.11.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

// MARK: - AppCoordinator

class AppCoordinator {
    
    weak var window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
        self.window?.makeKeyAndVisible()
    }
}

// MARK: - Coordinatable
extension AppCoordinator: Coordinatable {
    
    func start() {
        AppState.current { state in
            switch state {
            case .authorization:
                performAuthorizationFlow()
            case .student:
                performStudentFlow()
            case .teacher:
                performTeacherFlow()
            case .emptyRole:
                performAuthorizationFlow()
            }
        }
    }
    
    // MARK: Authentication flow
    private func performAuthorizationFlow() {
        let authCoordinator = AuthCoordinator(window: window)
        authCoordinator.start()
    }
    
    // MARK: Student flow
    private func performStudentFlow() {
        let studentCoordinator = StudentCoordinator(window: window)
        studentCoordinator.start()
    }
    
    // MARK: Teacher flow
    private func performTeacherFlow() {
        let teacherCoordinator = TeacherCoordinator(window: window)
        teacherCoordinator.start()
    }
}
