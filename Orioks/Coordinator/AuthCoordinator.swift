//
//  AuthCoordinator.swift
//  Orioks
//
//  Created by Генрих Берайлик on 27.11.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

class AuthCoordinator: Coordinatable {
    
    private weak var window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }
    
 
    
    func start() {
        let authVC = AuthenticationVC()
        window?.rootViewController = authVC
    }
    
}
