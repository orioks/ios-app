//
//  StudentCoordinator.swift
//  Orioks
//
//  Created by Генрих Берайлик on 03.12.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

class StudentCoordinator: Coordinatable {
    
    private weak var window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func start() {
        AppDelegate.shared?.requestPushNotifications()
        let mainVC = OrioksTabBar(role: .student)
        window?.rootViewController = mainVC
    }
}
