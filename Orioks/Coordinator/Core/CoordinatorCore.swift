//
//  CoordinatorCore.swift
//  Orioks
//
//  Created by Генрих Берайлик on 27.11.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

typealias CompletionBlock = () -> ()

protocol Coordinatable: class {
    func start()
}
