//
//  UserRole.swift
//  Orioks
//
//  Created by Генрих Берайлик on 27.11.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

enum UserRole: String {
    
    // MARK: - Initialization
    
    init?(role: String?) {
        if role?.contains(UserRole.student.rawValue) == true {
            self = .student
        } else if role?.contains(UserRole.teacher.rawValue) == true {
            self = .teacher
        } else {
            return nil
        }
    }
    
    // MARK: - Values
    
    case student = "student"
    case teacher = "teacher"
}

struct UserRolesHolder: Decodable {
    let current: String
    let parent: String
    let list: [UserRoleItem]
}

struct UserRoleItem: Decodable {
    let role: String
    let description: String?
}
