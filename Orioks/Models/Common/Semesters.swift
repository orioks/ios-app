//
//  Semesters.swift
//  Orioks
//
//  Created by Генрих Берайлик on 06.02.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import Foundation

struct SemestersHolder: Decodable {
    var current: Int
    let list: [SemesterItem]
}

struct SemesterItem: Decodable {
    let id: Int
    let name: String
}

struct PatchedSemester: Decodable {
    let semester: Int
}
