//
//  Discipline.swift
//  Orioks
//
//  Created by Gena Beraylik on 02.10.2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation


struct DisciplineItem: Decodable {
    
    let id: Int
    let name: String
    let controlForm: String?
    let currentGrade: Double?
    let department: String?
    let examDate: String?
    let maxGrade: Int
    let teachers: [String]?

    private enum CodingKeys: String, CodingKey {
        case controlForm = "formControl"
        case currentGrade = "score"
        case maxGrade = "currentMaxScore"
        case department = "institute"
        case id, name, teachers, examDate
    }

    // MARK: - Initialization
    
    init(discpipline: Discipline) {
        id = Int(discpipline.id)
        name = discpipline.name ?? ""
        controlForm = discpipline.controlForm
        currentGrade = discpipline.currentGrade
        department = discpipline.department
        examDate = discpipline.examDate
        maxGrade = Int(discpipline.maxGrade)
        teachers = discpipline.getTeachers()
    }
    
    // MARK: - Functions
    
}
