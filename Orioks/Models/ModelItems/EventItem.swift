//
//  Event.swift
//  Orioks
//
//  Created by Gena Beraylik on 06.10.2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation

struct EventItem: Decodable {
    let id: Int?
    let alias: String?
    let type: String
    let name: String?
    let currentScore: Double?
    let maxScore: Double
    let week: Int
    
    // MARK: - Initialization
    
    init(event: Event) {
        id = nil
        alias = event.alias
        type = event.type ?? ""
        name = event.name
        currentScore = event.currentGrade
        maxScore = Double(event.maxGrade)
        week = Int(event.week)
    }
    
    init(id: Int, name: String, type: String, currentScore: Double?, maxScore: Double, week: Int) {
        self.id = id
        self.name = name
        self.alias = nil
        self.type = type
        self.currentScore = currentScore
        self.maxScore = maxScore
        self.week = week
    }
    
}

struct EventScoreItem: Codable {
    let score: Double?
}
