//
//  AcademicDebtItem.swift
//  Orioks
//
//  Created by Генрих Берайлик on 11/02/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

class AcademicDebtItem: Decodable {
    
    var id: Int
    var name: String
    var maxGrade: Double
    var controlForm: String?
    var currentGrade: Double?
    var department: String?
    var deadline: String?
    var consultationTime: [String] = []
    
    let teachers: [String]?
    
    private enum CodingKeys: String, CodingKey {
        case controlForm = "control_form"
        case currentGrade = "current_grade"
        case consultationTime = "consultation_schedule"
        case maxGrade = "max_grade"
        case id, name, department, teachers, deadline
    }
    
    // MARK: - Initialization
    
    init(debt: AcademicDebt) {
        id = Int(debt.id)
        name = debt.name ?? ""
        controlForm = debt.controlForm
        currentGrade = debt.currentGrade
        department = debt.department
        deadline = debt.deadline
        maxGrade = debt.maxGrade
        teachers = debt.getTeachers()
        if let consultation = debt.consultationTime {
            consultationTime.append(consultation)
        }
    }
    
}
