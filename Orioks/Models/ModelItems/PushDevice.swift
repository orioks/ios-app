//
//  PushDevice.swift
//  Orioks
//
//  Created by Генрих Берайлик on 07/10/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

// MARK: - PushData

struct PushDevice: Codable {
    var pushKey: String?
    var deviceUuid: String
    var deviceType = 1
    
    init(deviceUuid: String, pushKey: String?) {
        self.deviceUuid = deviceUuid
        self.pushKey = pushKey
    }
}
