//
//  Profile.swift
//  Orioks
//
//  Created by Gena Beraylik on 02.10.2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation

struct ProfileItem: Decodable {
    let id: Int
    let shortName: String
    let fullName: String
    let institute: Institute?
    let role: Role?
    
    let group: Group?
    let course: Int?
    let recordBookId: Int?
    let studyDirection: String?
    let studyProfile: String?
}

struct Group: Decodable {
    let id: Int
    let shortName: String
    let fullName: String
}

struct Institute: Decodable {
    let id: Int
    let shortName: String
    let fullName: String
}

struct Role: Decodable {
    let id: String
    let name: String
    let parentRole: String
}
