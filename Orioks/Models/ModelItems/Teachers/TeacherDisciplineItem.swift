//
//  TeacherDisciplineItem.swift
//  Orioks
//
//  Created by Генрих Берайлик on 17.12.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

struct TeacherDisciplineHolder: Decodable {
    let name: String
    let studyGroups: [String]
    let disciplines: [TeacherDisciplineItem]
}

struct TeacherDisciplineItem: Decodable {
    let id: Int
    let name: String
    let formControl: String
    var studyGroups: [StudentsGroup]
}

struct StudentsGroup: Decodable {
    let id: Int
    let type: Int
    var disciplineId: Int?
    let shortName: String
    let fullName: String
    let attributes: [GroupAttribute]?
    let numberStudents: Int?
    let group: ShortGroup?
    
    func getAttributesText() -> String? {
        guard let attributes = self.attributes, !attributes.isEmpty else {
            return nil
        }
        var result = ""
        
        for (index, attr) in attributes.enumerated() {
            result.append(contentsOf: attr.shortName)
            if index < attributes.count-1 {
                result.append(contentsOf: ", ")
            }
        }
        return result
    }
}

struct ShortGroup: Decodable {
    let id: Int
    let fullName: String
}

struct GroupAttribute: Decodable {
    let shortName: String
    let fullName: String
}
