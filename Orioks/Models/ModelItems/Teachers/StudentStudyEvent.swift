//
//  StudentStudyEvent.swift
//  Orioks
//
//  Created by Генрих Берайлик on 29.12.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

struct StudentStudyEvent: Decodable {
    let id: Int
    let week: Int
    let name: String?
    let type: String
    let maxScore: Int
    let currentScore: Double?
}

