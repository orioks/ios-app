//
//  StudentStudyResult.swift
//  Orioks
//
//  Created by Генрих Берайлик on 29.12.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

struct StudentStudyResultItem: Decodable {
    let id: Int
    let fullName: String
    let totalScore: Double
    let currentScore: Double?
    let currentGrade: Int?
}

struct GroupStudentResultItem: Decodable {
    let id: Int
    let shortName: String
    let fullName: String
    let students: [StudentStudyResultItem]
}
