//
//  ResitItem.swift
//  Orioks
//
//  Created by Генрих Берайлик on 18/02/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

class ResitItem: Decodable {
    var debtId: Int?
    var classroom: String
    var datetime: String
    var resitNumber: Int
    
    private enum CodingKeys: String, CodingKey {
        case resitNumber = "resit_number"
        case debtId, classroom, datetime
    }
    
    // MARK: - Initialization
    
    init(resit: DebtResit) {
        debtId = Int(resit.debtId)
        classroom = resit.classroom ?? ""
        datetime = resit.datetime ?? ""
        resitNumber = Int(resit.resitNumber)
    }
    
}
