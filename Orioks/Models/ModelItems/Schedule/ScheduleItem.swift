//
//  ScheduleItem.swift
//  Orioks
//
//  Created by Генрих Берайлик on 17/07/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

// MARK: - Models for table view

struct ScheduleDataSource {
    var sections: [ScheduleSectionItem] = []
}

struct ScheduleSectionItem {
    var title: String?
    var rows: [ScheduleItemViewModel]
}

// MARK: - API Schedule

struct ScheduleItem: Codable {
    var name: String
    var type: String
    var day: Int
    var order: Int
    var week: Int
    var weekRecurrence: Int
    var location: String
    var teacher: String
    
    private enum CodingKeys: String, CodingKey {
        case order = "class"
        case weekRecurrence = "week_recurrence"
        case name, type, day, week, location, teacher
    }
}

struct StudyGroup: Decodable {
    let id: Int
    let name: String
}

struct SemesterInfo: Decodable {
    let semesterStart: String?
    let sessionStart: String?
    let sessionEnd: String?
    let nextSemesterStart: String?
}

// MARK: - CurrentDay

class CurrentScheduleDay {
    var day: WeekDay
    var week: WeekType
    
    init(day: WeekDay, week: WeekType) {
        self.day = day
        self.week = week
    }
    
    func nextWeek() {
        let nextWeek = WeekType(rawValue: week.rawValue+1)
        week = nextWeek ?? .firstNumerator
    }
    
    func nextDay() {
        let nextDay = WeekDay(rawValue: day.rawValue+1)
        if let nextDay = nextDay {
            day = nextDay
        } else {
            day = .monday
            nextWeek()
        }
    }
    
}
