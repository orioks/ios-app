//
//  Timetable.swift
//  Orioks
//
//  Created by Генрих Берайлик on 12.07.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import Foundation

struct Timetable {
    var order: Int
    var startTime: String
    var endTime: String
    
    fileprivate init(order: Int, startTime: String, endTime: String) {
        self.order = order
        self.startTime = startTime
        self.endTime = endTime
    }
}

extension Timetable {
    static func getBy(order: Int) -> Timetable? {
        switch order {
        case 1: return Timetable(order: order, startTime: "9:00", endTime: "10:30")
        case 2: return Timetable(order: order, startTime: "10:40", endTime: "12:10")
        case 3: return Timetable(order: order, startTime: "12:20", endTime: "13:50")
        case 4: return Timetable(order: order, startTime: "14:30", endTime: "16:00")
        case 5: return Timetable(order: order, startTime: "16:10", endTime: "17:40")
        case 6: return Timetable(order: order, startTime: "18:20", endTime: "19:50")
        case 7: return Timetable(order: order, startTime: "20:00", endTime: "21:30")
        default: return nil
        }
    }
}

// MARK: - Week types

enum WeekType: Int, CaseIterable {
    case firstNumerator = 0
    case firstDenumerator = 1
    case secondNumerator = 2
    case secondDenumerator = 3
}

extension WeekType {
    func toString() -> String {
        switch self {
        case .firstNumerator: return "Первый числитель"
        case .firstDenumerator: return "Первый знаменатель"
        case .secondNumerator: return "Второй числитель"
        case .secondDenumerator: return "Второй знаменатель"
        }
    }
}

enum WeekDay: Int, CaseIterable {
    case monday = 1
    case tuesday = 2
    case wednesday = 3
    case thursday = 4
    case friday = 5
    case saturday = 6
    case sunday = 7
}

extension WeekDay {
    func toString() -> String {
        switch self {
        case .monday: return "Понедельник"
        case .tuesday: return "Вторник"
        case .wednesday: return "Среда"
        case .thursday: return "Четверг"
        case .friday: return "Пятница"
        case .saturday: return "Суббота"
        case .sunday: return "Воскресенье"
        }
    }
}
