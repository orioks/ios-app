//
//  ScheduleItemModel.swift
//  Orioks
//
//  Created by Генрих Берайлик on 12.07.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import Foundation

struct ScheduleDataModel: Decodable {
    let data: [ScheduleItemModel]?
    
    private enum CodingKeys: String, CodingKey {
        case data = "Data"
    }
}

struct ScheduleItemModel: Codable {
    
    // MARK: - Properties
    
    let day: Int
    let dayNumber: Int
    
    let className: String
    let classTeacher: String
    let classTeacherFull: String
    
    let groupName: String
    let roomName: String
    
    let timeCode: Int
    let timeName: String
    
    // MARK: - Private Properties
    
    let classItem: Data? = nil
    let group: Data? = nil
    let room: Data? = nil
    let time: Data? = nil
    
    // MARK: - Coding Keys
    
    private enum CodingKeys: String, CodingKey {
        case day = "Day"
        case dayNumber = "DayNumber"
        case classItem = "Class"
        case group = "Group"
        case room = "Room"
        case time = "Time"
    }
    private enum ClassCodingKeys: String, CodingKey {
        case className = "Name"
        case classTeacher = "Teacher"
        case classTeacherFull = "TeacherFull"
    }
    private enum GroupCodingKeys: String, CodingKey {
        case groupName = "Name"
    }
    private enum RoomCodingKeys: String, CodingKey {
        case roomName = "Name"
    }
    private enum TimeCodingKeys: String, CodingKey {
        case timeCode = "Code"
        case timeName = "Time"
    }

    // MARK: - Initialization
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.day = try container.decode(Int.self, forKey: .day)
        self.dayNumber = try container.decode(Int.self, forKey: .dayNumber)

        // Class row
        let classContainer = try container.nestedContainer(keyedBy: ClassCodingKeys.self, forKey: .classItem)
        self.className = try classContainer.decode(String.self, forKey: .className)
        self.classTeacher = try classContainer.decode(String.self, forKey: .classTeacher)
        self.classTeacherFull = try classContainer.decode(String.self, forKey: .classTeacherFull)

        // Group row
        let groupContainer = try container.nestedContainer(keyedBy: GroupCodingKeys.self, forKey: .group)
        self.groupName = try groupContainer.decode(String.self, forKey: .groupName)

        // Room row
        let roomContainer = try container.nestedContainer(keyedBy: RoomCodingKeys.self, forKey: .room)
        self.roomName = try roomContainer.decode(String.self, forKey: .roomName)

        // Time row
        let timeContainer = try container.nestedContainer(keyedBy: TimeCodingKeys.self, forKey: .time)
        self.timeCode = try timeContainer.decode(Int.self, forKey: .timeCode)
        self.timeName = try timeContainer.decode(String.self, forKey: .timeName)
    }
    
    // MARK: - Encoding
    
    func encode(to encoder: Encoder) throws {
        var baseContainer = encoder.container(keyedBy: CodingKeys.self)
        try baseContainer.encode(day, forKey: .day)
        try baseContainer.encode(dayNumber, forKey: .dayNumber)
        
        // Class container
        var classContainer = baseContainer.nestedContainer(keyedBy: ClassCodingKeys.self, forKey: .classItem)
        try classContainer.encode(className, forKey: .className)
        try classContainer.encode(classTeacher, forKey: .classTeacher)
        try classContainer.encode(classTeacherFull, forKey: .classTeacherFull)
        
        // Group container
        var groupContainer = baseContainer.nestedContainer(keyedBy: GroupCodingKeys.self, forKey: .group)
        try groupContainer.encode(groupName, forKey: .groupName)
        
        // Room container
        var roomContainer = baseContainer.nestedContainer(keyedBy: RoomCodingKeys.self, forKey: .room)
        try roomContainer.encode(roomName, forKey: .roomName)
        
        // Time container
        var timeContainer = baseContainer.nestedContainer(keyedBy: TimeCodingKeys.self, forKey: .time)
        try timeContainer.encode(timeCode, forKey: .timeCode)
        try timeContainer.encode(timeName, forKey: .timeName)
    }
    
}
