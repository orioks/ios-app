//
//  DebtResit.swift
//  Orioks
//
//  Created by Генрих Берайлик on 18/02/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

extension DebtResit {
    
    func parseFromItem(_ item: ResitItem) {
        debtId = Int64(item.debtId ?? 0)
        classroom = item.classroom
        datetime = item.datetime
        resitNumber = Int32(item.resitNumber)
    }
    
    func parseToItem() -> ResitItem {
        return ResitItem(resit: self)
    }
    
    // MARK: - Creating data
    
    static func createResits(items: [ResitItem], debtId: Int) {
        readResits(debtId: debtId, completion: { (resits) in
            if !resits.isEmpty {
                deleteDebtResits(debtId)
            }
            for item in items {
                item.debtId = debtId
                let new = DebtResit(context: CoreDataService.context)
                new.parseFromItem(item)
            }
        })
    }
    
    // MARK: - Reading data
    
    static func readResits(debtId: Int, completion: @escaping ([DebtResit]) -> Void) {
        let allResits = CoreDataService.safelyFetchRequest(DebtResit.fetchRequest())
        let filtered = allResits.filter({ $0.debtId == debtId })

        completion(filtered)
    }
    
    static func readResitItems(debtId: Int, completion: @escaping ([ResitItem]) -> Void) {
        let allResits = CoreDataService.safelyFetchRequest(DebtResit.fetchRequest())
        let filtered = allResits.filter({ $0.debtId == debtId })
        
        var output: [ResitItem] = []
        filtered.forEach { (value) in
            let valueItem = ResitItem(resit: value)
            output.append(valueItem)
        }
        completion(output)
    }
    
    // MARK: - Updating data
    
    static func updateEvent(item: ResitItem) {
        print("cannot update item")
    }
    
    // MARK: - Deleting data
    
    static func deleteDebtResits(_ debtId: Int) {
        let allResits = CoreDataService.safelyFetchRequest(DebtResit.fetchRequest())
        let filtered = allResits.filter({ $0.debtId == debtId })
    
        filtered.forEach({ (event) in
            deleteResit(event)
        })
    }
    
    static func deleteResit(_ value: DebtResit) {
        CoreDataService.context.delete(value)
    }
    
    static func deleteAll() {
        let allRecords = CoreDataService.safelyFetchRequest(DebtResit.fetchRequest())
        allRecords.forEach({ (value) in
            self.deleteResit(value)
        })
    }
    
}
