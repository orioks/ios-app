//
//  Discipline+CDE.swift
//  Orioks
//
//  Created by Gena Beraylik on 07/12/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation

extension Discipline {
    
    func parseFromItem(_ item: DisciplineItem) {
        id = Int64(item.id)
        name = item.name
        controlForm = item.controlForm
        currentGrade = item.currentGrade ?? 0
        department = item.department
        examDate = item.examDate
        maxGrade = Int32(item.maxGrade)
        setTeachers(item.teachers)
    }
    
    func parseToItem() -> DisciplineItem {
        return DisciplineItem(discpipline: self)
    }
    
    func setTeachers(_ teachers: [String]?) {
        guard let teachers = teachers else {
            self.teachers = nil
            return
        }
        
        var output: String = ""
        for teacher in teachers {
            output.append(contentsOf: "\(teacher);")
        }
        output.removeLast()
        self.teachers = output
    }
    
    func getTeachers() -> [String]? {
        return self.teachers?.components(separatedBy: ";")
    }
    
    // MARK: - Creating data
    
    static func createDiscipline(item: DisciplineItem) {
        readDiscipline(id: item.id, completion: { (discipline) in
            if discipline != nil {
                updateDiscipline(item: item)
            } else {
                let new = Discipline(context: CoreDataService.context)
                new.parseFromItem(item)
            }
        })
    }
    
    // MARK: - Reading data
    
    static func readDisciplines(completion: @escaping ([Discipline]) -> Void) {
        let disciplines = CoreDataService.safelyFetchRequest(Discipline.fetchRequest())
        completion(disciplines)
    }
    
    static func readDiscipline(id: Int, completion: @escaping (Discipline?) -> Void) {
        let disciplines = CoreDataService.safelyFetchRequest(Discipline.fetchRequest())
        let result = disciplines.first(where: { $0.id == id })
        completion(result)
    }
    
    static func readDisciplineItem(id: Int, completion: @escaping (DisciplineItem?) -> Void) {
        let disciplines = CoreDataService.safelyFetchRequest(Discipline.fetchRequest())
        let result = disciplines.first(where: { $0.id == id })
        completion(result?.parseToItem())
    }
    
    static func readDisciplineItems(completion: @escaping ([DisciplineItem]) -> Void) {
        let disciplines = CoreDataService.safelyFetchRequest(Discipline.fetchRequest())
        
        var output: [DisciplineItem] = []
        disciplines.forEach { (discipline) in
            output.append(discipline.parseToItem())
        }
        completion(output)
    }
    
    // MARK: - Updating data
    
    static func updateDiscipline(item: DisciplineItem) {
        readDiscipline(id: item.id) { (discipline) in
            guard let discipline = discipline else { return }
            
            discipline.parseFromItem(item)
            
            CoreDataService.saveContext()
        }
    }
    
    // MARK: - Deleting data
    
    static func deleteDiscipline(_ value: Discipline) {
        CoreDataService.context.delete(value)
    }
    
    static func deleteAll() {
        let allRecords = CoreDataService.safelyFetchRequest(Discipline.fetchRequest())
        allRecords.forEach({ (value) in
            self.deleteDiscipline(value)
        })
    }
    
}
