//
//  Profile+CDE.swift
//  Orioks
//
//  Created by Gena Beraylik on 07/12/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation

extension Profile {
    
    // MARK: - Creating data
    
    static func createProfile(_ profileItem: ProfileItem) {
        readProfile(completion: { (profile) in
            if let profile = profile {
                deleteProfile(profile)
            }
        })
    }
    
    // MARK: - Reading data
    
    static func readProfile(completion: @escaping (Profile?) -> Void) {
        let profile = CoreDataService.safelyFetchRequest(Profile.fetchRequest())
        let result = profile.first
        completion(result)
    }
    
    static func readProfileItem(completion: @escaping (ProfileItem?) -> Void) {
        let profile = CoreDataService.safelyFetchRequest(Profile.fetchRequest())
        guard let _ = profile.first else {
            completion(nil)
            return
        }
        completion(nil)
    }
    
    // MARK: - Updating data
    
    static func updateProfile(_ profile: Profile) {
        CoreDataService.saveContext()
    }
    
    static func updateProfileItem(_ profile: ProfileItem) {
        CoreDataService.saveContext()
    }
    
    // MARK: - Deleting data
    
    static func deleteProfile(_ value: Profile) {
        CoreDataService.context.delete(value)
    }
    
    static func deleteAll() {
        let allRecords = CoreDataService.safelyFetchRequest(Profile.fetchRequest())
        allRecords.forEach({ (value) in
            self.deleteProfile(value)
        })
    }
    
}
