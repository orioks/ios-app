//
//  Events+CDE.swift
//  Orioks
//
//  Created by Генрих Берайлик on 11/12/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation

extension Event {
    
    func parseFromItem(_ item: EventItem) {
        alias = item.alias
        type = item.type
        name = item.name
        currentGrade = item.currentScore ?? 0
        maxGrade = Int32(item.maxScore)
        week = Int16(item.week)
    }
    
    func parseToItem() -> EventItem {
        return EventItem(event: self)
    }
    
    // MARK: - Creating data
    
    static func createEvents(items: [EventItem], for discipline: Discipline) {
        readEvents(discipline: discipline, completion: { (events) in
            if !events.isEmpty {
                deleteDisciplineEvents(discipline)
            }
            for item in items {
                let new = Event(context: CoreDataService.context)
                new.parseFromItem(item)
                new.discipline = discipline
            }
        })
    }
    
    // MARK: - Reading data
    
    static func readEvents(discipline: Discipline, completion: @escaping ([Event]) -> Void) {
        guard let events = discipline.events?.allObjects as? [Event] else {
            completion([])
            return
        }
        completion(events)
    }
    
    static func readEventItems(disciplineId: Int, completion: @escaping ([EventItem]) -> Void) {
        var output: [EventItem] = []
        Discipline.readDiscipline(id: disciplineId) { (discipline) in
            guard let discipline = discipline else {
                completion(output)
                return
            }
            readEvents(discipline: discipline, completion: { (events) in
                events.forEach({ (event) in
                    output.append(event.parseToItem())
                })
                output.sort(by: { $0.week < $1.week})
                completion(output)
            })
        }
    }
    
    // MARK: - Updating data
    
    static func updateEvent(item: EventItem) {
        print("cannot update item")
    }
    
    // MARK: - Deleting data
    
    static func deleteDisciplineEvents(_ discipline: Discipline) {
        guard let events = discipline.events?.allObjects as? [Event] else { return }
        events.forEach({ (event) in
            deleteEvent(event)
        })
    }
    
    static func deleteEvent(_ value: Event) {
        CoreDataService.context.delete(value)
    }
    
    static func deleteAll() {
        let allRecords = CoreDataService.safelyFetchRequest(Event.fetchRequest())
        allRecords.forEach({ (value) in
            self.deleteEvent(value)
        })
    }
    
}
