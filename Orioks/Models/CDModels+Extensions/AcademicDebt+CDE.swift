//
//  AcademicDebt+CDE.swift
//  Orioks
//
//  Created by Генрих Берайлик on 11/02/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

extension AcademicDebt {
    
    func parseFromItem(_ item: AcademicDebtItem) {
        id = Int64(item.id)
        name = item.name
        controlForm = item.controlForm
        currentGrade = item.currentGrade ?? 0
        department = item.department
        deadline = item.deadline
        maxGrade = item.maxGrade
        consultationTime = item.consultationTime.first
        setTeachers(item.teachers)
    }
    
    func parseToItem() -> AcademicDebtItem {
        return AcademicDebtItem(debt: self)
    }
    
    func setTeachers(_ teachers: [String]?) {
        guard let teachers = teachers else {
            self.teachers = nil
            return
        }
        
        var output: String = ""
        for teacher in teachers {
            output.append(contentsOf: "\(teacher);")
        }
        output.removeLast()
        self.teachers = output
    }
    
    func getTeachers() -> [String]? {
        return self.teachers?.components(separatedBy: ";")
    }
    
    // MARK: - Creating data
    
    static func createAcademicDebt(item: AcademicDebtItem) {
        readAcademicDebt(id: item.id, completion: { (academicDebt) in
            if academicDebt != nil {
                updateAcademicDebt(item: item)
            } else {
                let new = AcademicDebt(context: CoreDataService.context)
                new.parseFromItem(item)
            }
        })
    }
    
    // MARK: - Reading data
    
    static func readAcademicDebts(completion: @escaping ([AcademicDebt]) -> Void) {
        let AcademicDebts = CoreDataService.safelyFetchRequest(AcademicDebt.fetchRequest())
        completion(AcademicDebts)
    }
    
    static func readAcademicDebt(id: Int, completion: @escaping (AcademicDebt?) -> Void) {
        let AcademicDebts = CoreDataService.safelyFetchRequest(AcademicDebt.fetchRequest())
        let result = AcademicDebts.first(where: { $0.id == id })
        completion(result)
    }
    
    static func readAcademicDebtItem(id: Int, completion: @escaping (AcademicDebtItem?) -> Void) {
        let AcademicDebts = CoreDataService.safelyFetchRequest(AcademicDebt.fetchRequest())
        let result = AcademicDebts.first(where: { $0.id == id })
        completion(result?.parseToItem())
    }
    
    static func readAcademicDebtItems(completion: @escaping ([AcademicDebtItem]) -> Void) {
        let AcademicDebts = CoreDataService.safelyFetchRequest(AcademicDebt.fetchRequest())
        
        var output: [AcademicDebtItem] = []
        AcademicDebts.forEach { (AcademicDebt) in
            output.append(AcademicDebt.parseToItem())
        }
        completion(output)
    }
    
    // MARK: - Updating data
    
    static func updateAcademicDebt(item: AcademicDebtItem) {
        readAcademicDebt(id: item.id) { (AcademicDebt) in
            guard let AcademicDebt = AcademicDebt else { return }
            
            AcademicDebt.parseFromItem(item)
            
            CoreDataService.saveContext()
        }
    }
    
    // MARK: - Deleting data
    
    static func deleteAcademicDebt(_ value: AcademicDebt) {
        CoreDataService.context.delete(value)
    }
    
    static func deleteAll() {
        let allRecords = CoreDataService.safelyFetchRequest(AcademicDebt.fetchRequest())
        allRecords.forEach({ (value) in
            self.deleteAcademicDebt(value)
        })
    }
    
}
