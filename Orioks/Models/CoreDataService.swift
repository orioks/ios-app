//
//  CoreDataService.swift
//  Orioks
//
//  Created by Gena Beraylik on 07/12/2018.
//  Copyright © 2018 MIET. All rights reserved.
//


import Foundation
import CoreData

class CoreDataService {
    
    // MARK: - Core Data activity
    
    private init() {}
    
    static var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "OrioksModel")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error),") // \(error.userInfo)")
            }
        })
        return container
    }()
    
    static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    static func safelyFetchRequest<T: NSManagedObject>(_ request: NSFetchRequest<T>) -> [T] {
        do {
            let result = try context.fetch(request)
            return result
        } catch {
            print("Unable safely fetch CoreData")
            return []
        }
    }
    
    static func clearDataBase() {
        Profile.deleteAll()
        Discipline.deleteAll()
        Event.deleteAll()
        
        saveContext()
    }
    
}
