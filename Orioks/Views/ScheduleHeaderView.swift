//
//  ScheduleHeaderView.swift
//  Orioks
//
//  Created by Генрих Берайлик on 28/07/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

// MARK: - ScheduleHeader Delegate

protocol ScheduleHeaderDelegate: class {
    func didSelectScheduleFilter(_ filter: ScheduleFilter)
}

// MARK: - ScheduleHeaderView

class ScheduleHeaderView: UIView {
    
    // MARK: - Properties
    
    private var collectionView: UICollectionView!
    weak var delegate: ScheduleHeaderDelegate?
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCollectionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Interactions
    
    func setSelected(index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
    }
    
    // MARK: - Configure UI
    
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = frame.size
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(ScheduleFilterCell.self, forCellWithReuseIdentifier: ScheduleFilterCell.cellId)
        
        // add collection view
        addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
}

// MARK: - CollectionView Delegate

extension ScheduleHeaderView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectScheduleFilter(ScheduleFilter.allCases[indexPath.row])
    }
}

// MARK: - CollectionView DataSource

extension ScheduleHeaderView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ScheduleFilter.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ScheduleFilterCell.cellId, for: indexPath) as! ScheduleFilterCell
        cell.titleLabel.text = "\(ScheduleFilter.allCases[indexPath.row].rawValue)"
        return cell
    }
    
}
