//
//  ProfileHeaderView.swift
//  Orioks
//
//  Created by Генрих Берайлик on 01/12/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ProfileHeaderView: UIView {
    
    var profile: ProfileItem? {
        didSet {
             updateData()
        }
    }
    
    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
         setupViews()
         prepareData()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public functions

    func setProfileImage(image: UIImage) {
        profileImage.image = image
    }

    func setBackgroundImage(image: UIImage) {
        backgroundImage.image = image
    }

    // MARK: - Data management

    private func prepareData() {
        
        backgroundImage.image = UIImage.init(named: "miet_bg")
        profileImage.image = UIImage(named: "user")
    }
    
    func updateData() {
        nameLabel.text = profile?.fullName
        subNameLabel.text = profile?.group?.shortName
    }

    // MARK: - Configure appearance

    private func setupViews() {
        addSubview(backgroundImage)
        backgroundImage.addSubview(transBlackView)
        
        transBlackView.addSubview(profileImage)
        
        transBlackView.addSubview(stackView)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(subNameLabel)

        setupConstraints()
    }

    private func setupConstraints() {
        backgroundImage.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        transBlackView.snp.makeConstraints { (make) in
            make.edges.equalTo(backgroundImage)
        }
        
        profileImage.snp.makeConstraints { (make) in
            make.top.leading.equalToSuperview().offset(16)
            make.height.width.equalTo(transBlackView.snp.height).dividedBy(2.5)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage.snp.bottom).offset(8)
            make.leading.trailing.bottom.equalToSuperview().inset(UIEdgeInsets(top: 16, left: 16, bottom: 8, right: 16))
        }
        
    }

    // MARK: - UI Elements

    let transBlackView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    private let profileImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let backgroundImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()

    let stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.spacing = 16
        view.distribution = .fillProportionally
        return view
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 24)
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 2
        label.lineBreakMode = .byTruncatingTail
        label.textColor = .white
        return label
    }()

    private let subNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = .white
        return label
    }()
}
