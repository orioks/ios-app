//
//  DisciplineDetailsView.swift
//  Orioks
//
//  Created by Gena Beraylik on 21/10/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class DisciplineDetailsView: UIView {
    
    var discipline: DisciplineItem? {
        didSet {
            upadateViewData()
        }
    }
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        addConstranits()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    func upadateViewData() {
        guard let discipline = discipline else { return }
        
        controlLabel.text = discipline.controlForm
        teacherLabel.text = getTeachersText(teachers: discipline.teachers ?? [])
        detailsLabel.text = discipline.department
        
        if let date = parseDateString(dateStr: discipline.examDate ?? ""), date != "" {
            controlLabel.text?.append(" - \(date)")
        }
        
    }
    
    func getTeachersText(teachers: [String]) -> String {
        var result = ""
        for teacher in teachers {
            result += teacher + "\n"
        }
        if result.hasSuffix("\n") {
            result.removeLast()
        }
        
        return result
    }
    
    private func parseDateString(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let date = dateFormatter.date(from: dateStr)
        if let date = date {
            dateFormatter.dateFormat = "dd.MM.yyyy"
            let dateResult = dateFormatter.string(from: date )
  
            return dateResult
        } else {
            return nil
        }
    }
    
    // MARK: - Configure appearance
    
    func setupViews() {
        teacherImg.image = UIImage(named: "teacher")?.withRenderingMode(.alwaysTemplate)
        controlImg.image = UIImage(named: "test")?.withRenderingMode(.alwaysTemplate)
        detailsImg.image = UIImage(named: "college")?.withRenderingMode(.alwaysTemplate)
        
        // set color tint for iOS 13
        if #available(iOS 13.0, *) {
            detailsImg.tintColor = UIColor.systemBlue
            controlImg.tintColor = UIColor.systemBlue
            teacherImg.tintColor = UIColor.systemBlue
        }
    }
    
    func addSubviews() {
        addSubview(controlLabel)
        addSubview(teacherLabel)
        addSubview(detailsLabel)
        
        addSubview(controlImg)
        addSubview(teacherImg)
        addSubview(detailsImg)
    }
    
    func addConstranits() {
        let imgSize: CGFloat = 24
        
        // Control type row
        controlImg.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        controlImg.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        controlImg.widthAnchor.constraint(equalToConstant: imgSize).isActive = true
        controlImg.heightAnchor.constraint(equalTo: controlImg.widthAnchor, multiplier: 1).isActive = true
        
        controlLabel.topAnchor.constraint(equalTo: controlImg.topAnchor).isActive = true
        controlLabel.leadingAnchor.constraint(equalTo: controlImg.trailingAnchor, constant: 16).isActive = true
        controlLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        // Teacher names row
        teacherImg.topAnchor.constraint(equalTo: controlLabel.bottomAnchor, constant: 16).isActive = true
        teacherImg.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        teacherImg.widthAnchor.constraint(equalToConstant: imgSize).isActive = true
        teacherImg.heightAnchor.constraint(equalTo: teacherImg.widthAnchor, multiplier: 1).isActive = true
        
        teacherLabel.topAnchor.constraint(equalTo: controlLabel.bottomAnchor, constant: 16).isActive = true
        teacherLabel.leadingAnchor.constraint(equalTo: teacherImg.trailingAnchor, constant: 16).isActive = true
        teacherLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        // Details row
        detailsImg.topAnchor.constraint(equalTo: teacherLabel.bottomAnchor, constant: 16).isActive = true
        detailsImg.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        detailsImg.widthAnchor.constraint(equalToConstant: imgSize).isActive = true
        detailsImg.heightAnchor.constraint(equalTo: detailsImg.widthAnchor, multiplier: 1).isActive = true
        
        detailsLabel.topAnchor.constraint(equalTo: teacherLabel.bottomAnchor, constant: 16).isActive = true
        detailsLabel.leadingAnchor.constraint(equalTo: detailsImg.trailingAnchor, constant: 16).isActive = true
        detailsLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        detailsLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
    }
    
    // MARK: - UI Elements
    
    private let controlImg: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let teacherImg: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let detailsImg: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let controlLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.font = UIFont.systemFont(ofSize: 18)
        return view
    }()
    
    private let teacherLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.font = UIFont.systemFont(ofSize: 18)
        return view
    }()
    
    private let detailsLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.font = UIFont.systemFont(ofSize: 18)
        return view
    }()
    
}
