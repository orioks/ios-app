//
//  InputFieldView.swift
//  Orioks
//
//  Created by Gena Beraylik on 10/11/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation
import UIKit

class InputFieldView: UIView {
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
        
        prepareData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Data management
    
    func prepareData() {
        fieldIcon.image = UIImage.init(named: "man")
        
        textField.font = UIFont.boldSystemFont(ofSize: 18)
        
        underlineView.backgroundColor = .lightGray
    }
    
    // MARK: - Configure appearance
    
    private func setupSubviews() {
        addSubview(fieldIcon)
        addSubview(textField)
        addSubview(underlineView)
        
        // Add constraints
        
        fieldIcon.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        fieldIcon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4).isActive = true
        fieldIcon.widthAnchor.constraint(equalTo: fieldIcon.heightAnchor).isActive = true
        fieldIcon.bottomAnchor.constraint(equalTo: underlineView.topAnchor, constant: -12).isActive = true
        
        textField.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        textField.leadingAnchor.constraint(equalTo: fieldIcon.trailingAnchor, constant: 16).isActive = true
        textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4).isActive = true
        textField.bottomAnchor.constraint(equalTo: underlineView.topAnchor, constant: -8).isActive = true
        
        underlineView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2).isActive = true
        underlineView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        underlineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
    }
    
    // MARK: - UI Elements
    
    let fieldIcon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let textField: UITextField = {
        let view = UITextField()
        view.textColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let underlineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
}
