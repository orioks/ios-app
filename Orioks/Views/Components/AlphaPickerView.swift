//
//  AlphaPickerView.swift
//  Orioks
//
//  Created by Генрих Берайлик on 02.02.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import UIKit

// MARK: - AlphaPicker View

final class AlphaPickerView: UIView {
    
    // MARK: - Properties
    
    private lazy var data: [String] = []
    private var selectCompletion: ((Int) -> Void)?
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    
    deinit {
        print("Picker deinit")
    }
    
    // MARK: - Interactions
    
    func show(with data: [String], selectedIndex: Int?, completion: @escaping (Int) -> Void) {
        self.data = data
        self.selectCompletion = completion
        if let selected = selectedIndex, selected < data.count, data.count > 1 {
            pickerView.selectRow(selected, inComponent: 0, animated: true)
        }
        textField.becomeFirstResponder()
    }
    
    func dissmiss() {
        textField.resignFirstResponder()
        data = []
        pickerView.reloadAllComponents()
    }
    
    // MARK: - Selectors
    
    @objc private func cancelTapped() {
        dissmiss()
    }
    
    @objc private func doneTapped() {
        let row = pickerView.selectedRow(inComponent: 0)
        selectCompletion?(row)
        dissmiss()
    }
    
    // MARK: - Configure UI
    
    private func setupViews() {
        setupToolbar()
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        textField.inputView = pickerView
        textField.inputAccessoryView = toolbar
        
        addSubview(textField)
    }
    
    private func setupToolbar() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))

        toolbar.items = [cancelButton, flexSpace, doneButton]
    }
    
    // MARK: - UI Elements
    
    private lazy var textField: UITextField = {
        let view = UITextField()
        return view
    }()
    
    private let pickerView: UIPickerView = {
        let view = UIPickerView()
        return view
    }()
    
    private let toolbar: UIToolbar = {
        let view = UIToolbar()
        view.autoresizingMask = .flexibleHeight
        view.barStyle = .default
        return view
    }()
    
}

// MARK: - UIPickerView DataSource & Delegate

extension AlphaPickerView: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
}
