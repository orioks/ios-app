//
//  ScoreCircleChart.swift
//  Orioks
//
//  Created by Gena Beraylik on 05/11/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation
import UIKit

class ScoreChart: UIView {
    
    // MARK: - Properties
    
    let shapeLayer = CAShapeLayer()
    
    let progressLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        view.numberOfLines = 2
        view.adjustsFontSizeToFitWidth = true
        view.adjustsFontForContentSizeCategory = true
        return view
    }()
    
    var value: Double? {
        didSet {
            updateChart()
        }
    }
    
    var maxValue: Double = 100 {
        didSet {
            updateChart()
        }
    }
    
    var minValue: Double = 0 {
        didSet {
            updateChart()
        }
    }
    
    var chartHeight: CGFloat = 0 {
        didSet {
            setupChart()
        }
    }
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addProgressLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    private func updateChart() {
        setupChart()
        
        let baseFont: CGFloat = 32
        let valueAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: baseFont)]
        let subValueAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: baseFont/2.5)]
        
        let stringValue: String = value != nil ? String.deFloat(number: value!) : "-"
        let attrScore = NSMutableAttributedString.init(string: stringValue, attributes: valueAttributes)
        let attrSubscore = NSMutableAttributedString.init(string: "\nиз \(String.deFloat(number: maxValue))", attributes: subValueAttributes)
        
        attrScore.append(attrSubscore)
        progressLabel.attributedText = attrScore
        
        runGraph()
    }
    
    // MARK: - Interactions
    
    func runGraph() {
        let basicAnim = CABasicAnimation(keyPath: "strokeEnd")
        
        basicAnim.toValue =  (value ?? 0) / maxValue
        basicAnim.duration = 0.7
        
        basicAnim.fillMode = CAMediaTimingFillMode.forwards
        basicAnim.isRemovedOnCompletion = false
        
        shapeLayer.add(basicAnim, forKey: "draw")
    }
    
    // MARK: - Configure appearance
    
    func colorFor(score: Double, maxScore: Double) -> UIColor {
        let rate = score / maxScore
        
        switch rate {
        case _ where rate >= 0 && rate < 0.3:
            return UIColor.init(red: 255/255, green: 4/255, blue: 1/255, alpha: 1)
        case _ where rate >= 0.3 && rate < 0.5:
            return UIColor.init(red: 255/255, green: 106/255, blue: 48/255, alpha: 1)
        case _ where rate >= 0.5 && rate < 0.7:
            return UIColor.init(red: 239/255, green: 188/255, blue: 18/255, alpha: 1)
        case _ where rate >= 0.7 && rate < 0.86:
            return UIColor.init(red: 143/255, green: 197/255, blue: 45/255, alpha: 1)
        case _ where rate >= 0.86:
            return UIColor.init(red: 37/255, green: 200/255, blue: 1/255, alpha: 1)
        default:
            return UIColor.clear
        }
    }
    
    private func addProgressLabel() {
        self.addSubview(progressLabel)
        
        progressLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        progressLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        progressLabel.widthAnchor.constraint(equalTo: heightAnchor, multiplier: 0.7).isActive = true
    }
    
    private func setupChart() {
        let circularPath = UIBezierPath(arcCenter: .zero, radius: chartHeight/1.5, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        
        let centerPoint = CGPoint(x: chartHeight/2, y: chartHeight/2)
        
        // Adding progress circle
        shapeLayer.path = circularPath.cgPath
        shapeLayer.lineCap = .round
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = chartHeight / 8
        shapeLayer.strokeEnd = 0
        shapeLayer.strokeColor = colorFor(score: (value ?? 0), maxScore: maxValue).cgColor
        shapeLayer.position = centerPoint
        
        shapeLayer.transform = CATransform3DMakeRotation(-.pi/2, 0, 0, 1)
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func removeSublayers() {
        self.shapeLayer.removeFromSuperlayer()
    }
}
