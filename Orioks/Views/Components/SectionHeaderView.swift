//
//  DisciplineHeaderView.swift
//  Orioks
//
//  Created by Генрих Берайлик on 06.01.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import UIKit

final class SectionHeaderView: UIView {
    
    // MARK: - Properties
    
    var onExpandTap: ((_ isExpanded: Bool) -> Void)?
    
    // MARK: - Initialization
    
    init(title: String?, subtitle: String?, isExpanded: Bool = true) {
        super.init(frame: .zero)
        
        setupViews()
        titleLabel.text = title
        subtitleLabel.text = subtitle
        
        let image = isExpanded ? chevronDownImage : chevronUpImage
        expandButton.setImage(image, for: .normal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Selectors
    
    @objc private func chevronTapped(_ sender: UIButton) {
        if sender.currentImage == chevronUpImage {
            sender.setImage(chevronDownImage, for: .normal)
            onExpandTap?(true)
        } else if sender.currentImage == chevronDownImage {
            sender.setImage(chevronUpImage, for: .normal)
            onExpandTap?(false)
        }
    }
    
    // MARK: - Configure UI
    
    private func setupViews() {
        // setup superview
        if #available(iOS 13.0, *) {
            backgroundColor = .systemGray5
        } else {
            backgroundColor = .groupTableViewBackground
        }
        
        // setup button
        expandButton.setImage(chevronDownImage, for: .normal)
        expandButton.addTarget(self, action: #selector(chevronTapped(_:)), for: .touchUpInside)
        
        // add subviews
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(expandButton)
        
        // setup constraints
        titleLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(expandButton.snp.leading).offset(-8)
            make.top.leading.equalToSuperview().inset(UIEdgeInsets(top: 8, left: 16, bottom: 0, right: 16))
        }
        subtitleLabel.snp.makeConstraints { (make) in
            make.trailing.equalTo(expandButton.snp.leading).offset(-8)
            make.bottom.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 16, bottom: 4, right: 16))
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
        }
        expandButton.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-8)
            make.width.equalTo(32)
            make.centerY.equalToSuperview()
        }
    }
    
    // MARK: - UI Elements
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .right
        return label
    }()
    
    private let expandButton: UIButton = {
        let view = UIButton()
        view.tintColor = .darkGray
        return view
    }()
    
    private lazy var chevronUpImage: UIImage? = {
        let view = UIImage(named: "chevron-up")
        return view?.withRenderingMode(.alwaysTemplate)
    }()
    
    private lazy var chevronDownImage: UIImage? = {
        let view = UIImage(named: "chevron-down")
        return view?.withRenderingMode(.alwaysTemplate)
    }()
    
}
