//
//  DebtResitsView.swift
//  Orioks
//
//  Created by Генрих Берайлик on 19/02/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

class DebtResitsView: UIView {
    
    var debtItem: AcademicDebtItem? {
        didSet {
            upadateViewData()
        }
    }
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        addConstranits()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    func upadateViewData() {
        guard let debtItem = debtItem else { return }
        
        controlLabel.text = debtItem.controlForm
        teacherLabel.text = getTeachersText(teachers: debtItem.teachers ?? [])
        detailsLabel.text = debtItem.department
    }
    
    func getTeachersText(teachers: [String]) -> String {
        var result = ""
        for teacher in teachers {
            result += teacher + "\n"
        }
        if result.hasSuffix("\n") {
            result.removeLast()
        }
        return result
    }
    
    private func getConsultations(_ consultations: [String]) -> String {
        var result = ""
        for consultation in consultations {
            result += consultation + "\n"
        }
        if result.hasSuffix("\n") {
            result.removeLast()
        }
        return result
    }
    
    // MARK: - Configure appearance
    
    func setupViews() {
        teacherImg.image = UIImage(named: "teacher")
        controlImg.image = UIImage(named: "test")
        detailsImg.image = UIImage(named: "college")
    }
    
    func addSubviews() {
        addSubview(controlLabel)
        addSubview(teacherLabel)
        addSubview(detailsLabel)
        
        addSubview(controlImg)
        addSubview(teacherImg)
        addSubview(detailsImg)
    }
    
    func addConstranits() {
        let imgSize: CGFloat = 24
        
        // Control type row
        controlImg.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        controlImg.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        controlImg.widthAnchor.constraint(equalToConstant: imgSize).isActive = true
        controlImg.heightAnchor.constraint(equalTo: controlImg.widthAnchor, multiplier: 1).isActive = true
        
        controlLabel.topAnchor.constraint(equalTo: controlImg.topAnchor).isActive = true
        controlLabel.leadingAnchor.constraint(equalTo: controlImg.trailingAnchor, constant: 16).isActive = true
        controlLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        // Teacher names row
        teacherImg.topAnchor.constraint(equalTo: controlLabel.bottomAnchor, constant: 16).isActive = true
        teacherImg.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        teacherImg.widthAnchor.constraint(equalToConstant: imgSize).isActive = true
        teacherImg.heightAnchor.constraint(equalTo: teacherImg.widthAnchor, multiplier: 1).isActive = true
        
        teacherLabel.topAnchor.constraint(equalTo: controlLabel.bottomAnchor, constant: 16).isActive = true
        teacherLabel.leadingAnchor.constraint(equalTo: teacherImg.trailingAnchor, constant: 16).isActive = true
        teacherLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        // Details row
        detailsImg.topAnchor.constraint(equalTo: teacherLabel.bottomAnchor, constant: 16).isActive = true
        detailsImg.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        detailsImg.widthAnchor.constraint(equalToConstant: imgSize).isActive = true
        detailsImg.heightAnchor.constraint(equalTo: detailsImg.widthAnchor, multiplier: 1).isActive = true
        
        detailsLabel.topAnchor.constraint(equalTo: teacherLabel.bottomAnchor, constant: 16).isActive = true
        detailsLabel.leadingAnchor.constraint(equalTo: detailsImg.trailingAnchor, constant: 16).isActive = true
        detailsLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        detailsLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
    }
    
    // MARK: - UI Elements
    
    private let controlImg: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let teacherImg: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let detailsImg: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let controlLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.font = UIFont.systemFont(ofSize: 18)
        return view
    }()
    
    private let teacherLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.font = UIFont.systemFont(ofSize: 18)
        return view
    }()
    
    private let detailsLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.font = UIFont.systemFont(ofSize: 18)
        return view
    }()
    
}
