//
//  DiscplinesCell.swift
//  Orioks
//
//  Created by Gena Beraylik on 13/10/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation
import UIKit

class DisciplineCell: UITableViewCell {
    
    var discipline: DisciplineItem? {
        didSet {
            updateData()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubviews()
        addConstrants()
        updateData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        addSubview(nameLabel)
        addSubview(controlLabel)
        addSubview(scoreView)
        
        scoreView.addSubview(scoreLabel)
    }
    
    func addConstrants() {
        nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: scoreView.leadingAnchor, constant: -16).isActive = true
        
        controlLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4).isActive = true
        controlLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        controlLabel.trailingAnchor.constraint(equalTo: scoreView.leadingAnchor, constant: -16).isActive = true
        controlLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
        
        scoreView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        scoreView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        scoreView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        scoreView.heightAnchor.constraint(equalTo: scoreView.widthAnchor).isActive = true
        
        scoreLabel.centerYAnchor.constraint(equalTo: scoreView.centerYAnchor).isActive = true
        scoreLabel.centerXAnchor.constraint(equalTo: scoreView.centerXAnchor).isActive = true
        
        scoreView.layer.cornerRadius = 24
        scoreView.chartHeight = 48
    }
    
    func updateData() {
        guard let discipline = discipline else { return }
        updateContent(title: discipline.name,
                      subtitle: discipline.controlForm,
                      maxValue: Double(discipline.maxGrade),
                      currentValue: discipline.currentGrade)
        
    }
    
    func updateContent(title: String, subtitle: String?, maxValue: Double, currentValue: Double?) {
        nameLabel.text = title
        controlLabel.text = subtitle
        
        scoreView.maxValue = maxValue
        scoreView.value = currentValue
    }
    
    // MARK: - UI Elements
    
    let nameLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.systemFont(ofSize: 16)
        view.numberOfLines = 0
        view.adjustsFontSizeToFitWidth = true
        view.minimumScaleFactor = 0.8
        view.lineBreakMode = .byTruncatingTail
        return view
    }()
    
    let controlLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .gray
        view.font = UIFont.systemFont(ofSize: 16)
        return view
    }()
    
    let scoreLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let scoreView: ScoreChart = {
        let view = ScoreChart()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
}
