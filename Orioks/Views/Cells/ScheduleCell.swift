//
//  ScheduleCell.swift
//  Orioks
//
//  Created by Генрих Берайлик on 05/08/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit
import SnapKit

class ScheduleCell: UITableViewCell {

    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func prepareForReuse() {
        titleLabel.text = nil
        detailsLabel.text = nil
        timeLabel.text = nil
    }
    
    // MARK: - Interactions
    
    func setViewModel(_ viewModel: ScheduleItemViewModel) {
        titleLabel.text = viewModel.subjectTitle
        detailsLabel.text = viewModel.subjectDescription
        timeLabel.text = viewModel.timing
    }
    
    // MARK: - Configure UI
    
    private func setupViews() {
        // Add Subviews
        addSubview(titleLabel)
        addSubview(detailsLabel)
        addSubview(timeLabel)
        
        // Add Constraints
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(8)
            make.leading.equalTo(self).offset(16)
            make.trailing.equalTo(timeLabel.snp.leading).offset(-8)
        }
        detailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.leading.equalTo(self).offset(16)
            make.trailing.equalTo(titleLabel)
            make.bottom.equalTo(self).offset(-8)
        }
        timeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(8)
            make.trailing.equalTo(self).offset(-8)
            make.bottom.equalTo(self).offset(-8)
            make.width.equalTo(56)
        }
    }
    
    // MARK: - UI Elements
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = 0
        return label
    }()
    
    private let detailsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .darkGray
        label.numberOfLines = 0
        return label
    }()
    
    private let timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textAlignment = .right
        label.numberOfLines = 0
        return label
    }()
    
}
