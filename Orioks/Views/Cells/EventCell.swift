//
//  EventsCell.swift
//  Orioks
//
//  Created by Gena Beraylik on 13/10/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation
import UIKit

class EventCell: UITableViewCell {
    
    var event: EventItem? {
        didSet {
            updateEventData()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubviews()
        addConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        addSubview(eventLabel)
        addSubview(typeLabel)
        addSubview(weekLabel)
        addSubview(scoreLabel)
    }
    
    func addConstraints() {
        weekLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        weekLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        weekLabel.widthAnchor.constraint(equalToConstant: 48).isActive = true
        weekLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        
        scoreLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        scoreLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        scoreLabel.widthAnchor.constraint(equalToConstant: 48).isActive = true
        scoreLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        
        eventLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        eventLabel.leadingAnchor.constraint(equalTo: weekLabel.trailingAnchor, constant: 8).isActive = true
        eventLabel.trailingAnchor.constraint(equalTo: scoreLabel.leadingAnchor, constant: -8).isActive = true
        
        typeLabel.topAnchor.constraint(equalTo: eventLabel.bottomAnchor, constant: 8).isActive = true
        typeLabel.leadingAnchor.constraint(equalTo: weekLabel.trailingAnchor, constant: 8).isActive = true
        typeLabel.trailingAnchor.constraint(equalTo: scoreLabel.leadingAnchor, constant: -8).isActive = true
        typeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
    }
    
    private func updateEventData() {
        guard let event = event else { return }
        updateContent(week: event.week,
                      name: event.type,
                      type: event.alias ?? event.name,
                      currentScore: event.currentScore,
                      maxScore: event.maxScore)
    }
    
    func updateContent(week: Int, name: String?, type: String?, currentScore: Double?, maxScore: Double) {
        
        // Week label attributes
        let lineAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 22)]
        let subLineAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        
        let weekTitle = NSMutableAttributedString.init(string: "\(week)", attributes: lineAttributes)
        let weekSubtitle = NSMutableAttributedString.init(string: "\nнед.", attributes: subLineAttributes)
        
        weekTitle.append(weekSubtitle)
        
        // Score attributes
        var scoreTitle = ""
        var scoreColor = UIColor.black
        if #available(iOS 13.0, *) {
            scoreColor = UIColor.label
        }
        if let grade = currentScore {
            if grade < 0 {
                scoreColor = .red
                scoreTitle = "н"
            } else {
                scoreColor = scoreChart.colorFor(score: grade, maxScore: maxScore)
                scoreTitle = String.deFloat(number: grade)
            }
        } else {
            scoreTitle = "-"
        }
        
        let scoreAttrTitle = NSMutableAttributedString.init(string: scoreTitle, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 22), NSAttributedString.Key.foregroundColor: scoreColor])
        let scoreAttrSubtitle = NSMutableAttributedString.init(string: "\n из \(Int(maxScore))", attributes: subLineAttributes)
        
        scoreAttrTitle.append(scoreAttrSubtitle)
        
        eventLabel.text = name
        typeLabel.text = type
        weekLabel.attributedText = weekTitle
        scoreLabel.attributedText = scoreAttrTitle
        
        // Update chart data
        scoreChart.chartHeight = 48
        scoreChart.maxValue = maxScore
        scoreChart.value = currentScore ?? 0
    }
    
    // MARK: - UI Elements
    
    let scoreChart = ScoreChart()
    
    let eventLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.adjustsFontSizeToFitWidth = true
        view.minimumScaleFactor = 0.8
        view.lineBreakMode = .byTruncatingTail
        view.numberOfLines = 0
        return view
    }()
    
    let typeLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.systemFont(ofSize: 16)
        view.textColor = .darkGray
        view.numberOfLines = 0
        view.adjustsFontSizeToFitWidth = true
        view.minimumScaleFactor = 0.8
        view.lineBreakMode = .byTruncatingTail
        return view
    }()
    
    let weekLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .gray
        view.textAlignment = .center
        view.adjustsFontSizeToFitWidth = true
        view.font = UIFont.systemFont(ofSize: 16)
        view.lineBreakMode = .byTruncatingTail
        view.numberOfLines = 0
        return view
    }()
    
    let scoreLabel: UILabel = {
        let view = UILabel()
        view.adjustsFontSizeToFitWidth = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.lineBreakMode = .byTruncatingTail
        view.textAlignment = .center
        view.numberOfLines = 0
        return view
    }()
    
}
