//
//  AcademicDebtCell.swift
//  Orioks
//
//  Created by Генрих Берайлик on 19/02/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

class AcademicDebtCell: UITableViewCell {
    
    var debt: AcademicDebtItem? {
        didSet {
            updateData()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubviews()
        addConstrants()
        updateData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        addSubview(nameLabel)
        addSubview(controlLabel)
        addSubview(scoreView)
        
        scoreView.addSubview(scoreLabel)
    }
    
    func addConstrants() {
        nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: scoreView.leadingAnchor, constant: -16).isActive = true
        
        controlLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4).isActive = true
        controlLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        controlLabel.trailingAnchor.constraint(equalTo: scoreView.leadingAnchor, constant: -16).isActive = true
        controlLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
        
        scoreView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        scoreView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        scoreView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        scoreView.heightAnchor.constraint(equalTo: scoreView.widthAnchor).isActive = true
        
        scoreLabel.centerYAnchor.constraint(equalTo: scoreView.centerYAnchor).isActive = true
        scoreLabel.centerXAnchor.constraint(equalTo: scoreView.centerXAnchor).isActive = true
        
        scoreView.layer.cornerRadius = 24
    }
    
    func updateData() {
        guard let debt = debt else { return }
        
        nameLabel.text = debt.name
        controlLabel.text = debt.controlForm
        
        scoreView.maxValue = Double(debt.maxGrade)
        scoreView.value = debt.currentGrade
        scoreView.chartHeight = 48
    }
    
    // MARK: - UI Elements
    
    let nameLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.systemFont(ofSize: 16)
        view.numberOfLines = 0
        view.adjustsFontSizeToFitWidth = true
        view.minimumScaleFactor = 0.8
        view.lineBreakMode = .byTruncatingTail
        return view
    }()
    
    let controlLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .gray
        view.font = UIFont.systemFont(ofSize: 16)
        return view
    }()
    
    let scoreLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let scoreView: ScoreChart = {
        let view = ScoreChart()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
}
