//
//  ScheduleFilterCell.swift
//  Orioks
//
//  Created by Генрих Берайлик on 28/07/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

final class ScheduleFilterCell: UICollectionViewCell {
    
    // MARK: - Properties
    
    override var isSelected: Bool {
        didSet { 
            if isSelected {
                titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
            } else {
                titleLabel.font = UIFont.systemFont(ofSize: 18)
            }
        }
    }
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configure UI
    
    private func setupViews() {
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4).isActive = true
    }
    
    // MARK: - UI Elements
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
}
