//
//  Cache.swift
//  Orioks
//
//  Created by Генрих Берайлик on 27.11.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

class Cache {
    
    // MARK: - Properties
    
    internal class var userDefaults: UserDefaults {
        UserDefaults.standard
    }
    
    // MARK: - Interactions
    
    class func fetch<T>(_ key: Key) -> T? {
        return userDefaults.object(forKey: key.rawValue) as? T
    }
    
    class func put(value: Any?, for key: Key) {
        userDefaults.set(value, forKey: key.rawValue)
    }
    
    class func clear() {
        userDefaults.dictionaryRepresentation().keys.forEach(userDefaults.removeObject(forKey:))
    }
    
    // MARK: - Value keys
    
    enum Key: String {
        case token = "API_TOKEN"
        case userRole = "USER_ROLE"
        case semester = "SEMESTER"
        case semesterStart = "SEMESTER_START"
        case semesterDidChange = "SEMESTER_DID_CHANGE"
        case scheduleData = "SCHEDULE_DATA"
    }
}

// MARK: - Groupped Cache

final class GroupCache: Cache {
    
    override class var userDefaults: UserDefaults {
        UserDefaults(suiteName: "group.fgu.miet.orioks") ?? UserDefaults.standard
    }

}
