//
//  ApiService.swift
//  Orioks
//
//  Created by Gena Beraylik on 02.10.2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation

class ApiService {
    
    private let serverUrl = "https://orioks.miet.ru/api/v2"
    
    var deviceName: String = ""
    
    static let shared = ApiService()
    
    private init() {}
 
    // MARK: - Authorization routes
    
    func authRequest(login: String, password: String, completion: @escaping (_ response: [String: Any]) -> Void) {
        guard let pathUrl = URL(string: "\(serverUrl)/authorization") else { return }
        
        let credentials = "\(login):\(password)"
        let encrypted = credentials.toBase64()
        
        var request = URLRequest(url: pathUrl)
        request.httpMethod = "POST"
        request.addValue("Basic \(encrypted)", forHTTPHeaderField: "Authorization")
        request.addValue("orioks_ios/1.0 iOS12", forHTTPHeaderField: "User-Agent")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in

            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = data else { return }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                completion(json!)
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    func deleteAuthToken(completion: @escaping (_ response: Bool) -> Void) {
        guard let token: String = Cache.fetch(.token) else { return }
        guard let pathUrl = URL(string: "\(serverUrl)/student/tokens/\(token)") else { return }
        
        let request = prepareRequest("DELETE", url: pathUrl, token: token)
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            var hasError = true
            if let data = self.requestHandler(data: data, response: response, error: error) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                    let message = json?["info"] as? String
                    print(message ?? "")
                    hasError = false
                } catch let error {
                    print(error.localizedDescription)
                }
            } else {
                print("Could not delete token")
            }
            completion(!hasError)
        })
        task.resume()
    }
    
    // MARK: - Profile request
    
    func getStudentProfile(completion: @escaping (_ response: ProfileItem?) -> Void) {
        guard let pathUrl = URL(string: "\(serverUrl)/student") else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: ProfileItem?) in
            completion(resultData)
        }
    }
    
    func getTeacherProfile(completion: @escaping (_ response: ProfileItem?) -> Void) {
        guard let pathUrl = URL(string: "\(serverUrl)/teacher") else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: ProfileItem?) in
            completion(resultData)
        }
    }
    
    // MARK: - Semesters requests
    
    func getSemesters(completion: @escaping (SemestersHolder?) -> Void) {
        let baseUrl = "\(serverUrl)/semesters"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: SemestersHolder?) in
            completion(resultData)
        }
    }
    
    func patchSemester(_ semesterId: Int, completion: @escaping (PatchedSemester?) -> Void) {
        let baseUrl = "\(serverUrl)/semesters/\(semesterId)"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("PATCH", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: PatchedSemester?) in
            completion(resultData)
        }
    }
    
    // MARK: - Roles requests
    
    func getRoles(completion: @escaping (UserRolesHolder?) -> Void) {
        let baseUrl = "\(serverUrl)/roles"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: UserRolesHolder?) in
            completion(resultData)
        }
    }
    
    func patchRole(_ newRole: String, completion: @escaping (UserRoleItem?) -> Void) {
        let baseUrl = "\(serverUrl)/roles/\(newRole)"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("PATCH", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: UserRoleItem?) in
            completion(resultData)
        }
    }
    
    // MARK: - Discipline routes
    
    func getDisciplines(completion: @escaping (_ response: [DisciplineItem]?) -> Void) {
        let baseUrl = "\(serverUrl)/student/disciplines"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: [DisciplineItem]?) in
            completion(resultData)
        }
    }
    
    func getEvents(disciplineId: Int, completion: @escaping (_ response: [EventItem]?) -> Void) {
        let baseUrl = "\(serverUrl)/student/disciplines/\(disciplineId)/events"
    
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: [EventItem]?) in
            completion(resultData)
        }
    }
    
    // MARK: - Academic debts routes
    
    func getAcademicDebts(completion: @escaping (_ response: [AcademicDebtItem]?) -> Void) {
        let baseUrl = "\(serverUrl)/student/debts"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: [AcademicDebtItem]?) in
            completion(resultData)
        }
    }
    
    func getAcademicDebtResits(debtId: Int, completion: @escaping (_ response: [ResitItem]?) -> Void) {
        let baseUrl = "\(serverUrl)/student/debts/\(debtId)/events"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: [ResitItem]?) in
            completion(resultData)
        }
    }
    
    // MARK: - Remote pushes routes
    
    func registerPush(pushData: PushDevice, completion: @escaping (_ response: PushDevice?) -> Void) {
        let baseUrl = "\(serverUrl)/student/pushes/register"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        let body = try? JSONEncoder().encode(pushData)
        
        var request = prepareRequest("POST", url: pathUrl, token: token)
        request.httpBody = body

        performGeneric(request: request) { (resultData: PushDevice?) in
            completion(resultData)
        }
    }
    
    func unregisterPush(pushData: PushDevice) {
        let baseUrl = "\(serverUrl)/student/pushes/unregister"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        let body = try? JSONEncoder().encode(pushData)
        
        var request = prepareRequest("POST", url: pathUrl, token: token)
        request.httpBody = body
        
        performGeneric(request: request) { (resultData: PushDevice?) in }
    }
    
    // MARK: - Schedule routes
    
    func getSemesterInfo(completion: @escaping (_ response: SemesterInfo?) -> Void) {
        let baseUrl = "\(serverUrl)/schedule"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: SemesterInfo?) in
            completion(resultData)
        }
    }
    
    func getStudyGroups(completion: @escaping (_ response: [StudyGroup]?) -> Void) {
        let baseUrl = "\(serverUrl)/schedule/groups"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: [StudyGroup]?) in
            completion(resultData)
        }
    }
    
    func getSchedule(groupId: Int, completion: @escaping (_ response: [ScheduleItem]?) -> Void) {
        let baseUrl = "\(serverUrl)/schedule/groups/\(groupId)"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: [ScheduleItem]?) in
            completion(resultData)
        }
    }
    
    // MARK: - Schedule Common requests
    
    func getMietGroups(completion: @escaping (_ response: [String]?) -> Void) {
        let baseUrl = "https://miet.ru/schedule/groups"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("POST", url: pathUrl, token: token)
        
        performGeneric(request: request) { (resultData: [String]?) in
            completion(resultData)
        }
    }
    
    func getSchedule(groupName: String, completion: @escaping (_ response: ScheduleDataModel?) -> Void) {
        let baseUrl = "https://miet.ru/schedule/data"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        
        var request = URLRequest(url: pathUrl)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = "group=\(groupName)".data(using: .utf8)

        performGeneric(request: request) { (resultData: ScheduleDataModel?) in
            completion(resultData)
        }
    }
    
    // MARK: - Teachers requests
    
    func getTeacherDisciplines(completion: @escaping ([TeacherDisciplineHolder]?) -> Void) {
        let baseUrl = "\(serverUrl)/teacher/disciplines"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        performGeneric(request: request) { (resultData: [TeacherDisciplineHolder]?) in
            completion(resultData)
        }
    }
    
    func getDisciplneStudents(_ disciplineId: Int, groupId: Int, completion: @escaping ([StudentStudyResultItem]?) -> Void) {
        let baseUrl = "\(serverUrl)/teacher/disciplines/\(disciplineId)/groups/\(groupId)"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        performGeneric(request: request) { (resultData: GroupStudentResultItem?) in
            completion(resultData?.students)
        }
    }
    
    func getStudentStudyEvents(_ disciplineId: Int, groupId: Int, studentId: Int, completion: @escaping ([EventItem]?) -> Void) {
        let baseUrl = "\(serverUrl)/teacher/disciplines/\(disciplineId)/groups/\(groupId)/students/\(studentId)/events"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        let request = prepareRequest("GET", url: pathUrl, token: token)
        performGeneric(request: request) { (resultData: [EventItem]?) in
            completion(resultData)
        }
    }
    
    func updateStudentEventScore(studentId: Int, disciplineId: Int, groupId: Int, eventId: Int, newScore: Double, completion: @escaping (EventScoreItem?) -> Void) {
        let baseUrl = "\(serverUrl)/teacher/disciplines/\(disciplineId)/groups/\(groupId)/students/\(studentId)/events/\(eventId)"
        
        guard let pathUrl = URL(string: baseUrl) else { return }
        guard let token: String = Cache.fetch(.token) else { return }
        
        var request = prepareRequest("PUT", url: pathUrl, token: token)
        request.httpBody = try? JSONEncoder().encode(EventScoreItem(score: newScore))
        
        performGeneric(request: request) { (resultData: EventScoreItem?) in
            completion(resultData)
        }
    }
    
    // MARK: - Supporting methods
    
    private func prepareRequest(_ method: String, url: URL, token: String) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.addValue(setupUserAgent(), forHTTPHeaderField: "User-Agent")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }
    
    private func setupUserAgent() -> String {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        let os = ProcessInfo().operatingSystemVersion
        let osVersion = "\(os.majorVersion).\(os.minorVersion).\(os.patchVersion)"
        
        return "orioks_ios/\(appVersion ?? "-") iOS\(osVersion)/\(deviceName)"
    }
    
    private func performGeneric<T: Decodable>(request: URLRequest, completion: @escaping (T?) -> ()) {
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            guard let data = self.requestHandler(data: data, response: response, error: error) else {
                completion(nil)
                return
            }
            do {
                let resultData = try JSONDecoder().decode(T.self, from: data)
                completion(resultData)
            } catch let error {
                completion(nil)
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    private func requestHandler(data: Data?, response: URLResponse?, error: Error?) -> Data? {
        if let error = error {
            print(error.localizedDescription)
            return nil
        }
        
        guard let httpResponse = response as? HTTPURLResponse else { return nil }
        if httpResponse.statusCode == 200 {
            return data
        } else {
            if let data = data, let stringData = String.init(data: data, encoding: String.Encoding.utf8) {
                print(stringData)
            }
            return nil
        }
    }
    
}
