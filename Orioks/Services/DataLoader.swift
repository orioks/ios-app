//
//  DataLoader.swift
//  Orioks
//
//  Created by Миландр on 07/12/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation

class DataLoader {

    func loadProfile(completion: @escaping (_ profile: ProfileItem?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            ApiService.shared.getStudentProfile(completion: { (profileItem) in
                guard let profileItem = profileItem else {
                    completion(nil)
                    return
                }
                Profile.createProfile(profileItem)
                completion(profileItem)
            })
        } else {
            Profile.readProfile { (profile) in
            }
        }
    }
    
    func loadDisciplines(completion: @escaping (_ data: [DisciplineItem]?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            ApiService.shared.getDisciplines(completion: { (items) in
                for item in items ?? [] {
                    Discipline.createDiscipline(item: item)
                }
                completion(items)
            })
        } else {
            Discipline.readDisciplineItems { (disciplines) in
                completion(disciplines)
            }
        }
    }
    
    func loadEvents(disciplineId: Int, completion: @escaping (_ data: [EventItem]?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            ApiService.shared.getEvents(disciplineId: disciplineId, completion: { (eventItems) in
                guard let eventItems = eventItems else {
                    completion(nil)
                    return
                }
                Discipline.readDiscipline(id: disciplineId, completion: { (discipline) in
                    guard let discipline = discipline else {
                        completion(nil)
                        return
                    }
                    Event.createEvents(items: eventItems, for: discipline)
                    completion(eventItems)
                })
            })
        } else {
            Event.readEventItems(disciplineId: disciplineId, completion: { (events) in
                completion(events)
            })
        }
    }
    
    func loadAcademicDebts(completion: @escaping (_ data: [AcademicDebtItem]?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            ApiService.shared.getAcademicDebts() { (items) in
                for item in items ?? [] {
                    AcademicDebt.createAcademicDebt(item: item)
                }
                completion(items)
            }
        } else {
            AcademicDebt.readAcademicDebtItems() { (debts) in
                completion(debts)
            }
        }
    }
    
    func loadAcademicDebtResits(debtId: Int, completion: @escaping (_ data: [ResitItem]?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            ApiService.shared.getAcademicDebtResits(debtId: debtId) { (items) in
                DebtResit.createResits(items: items ?? [], debtId: debtId)
                completion(items)
            }
        } else {
            DebtResit.readResitItems(debtId: debtId) { (resits) in
                completion(resits)
            }
        }
    }
    
    // MARK: - Schedule Provider
    
    func getSemesterInfo(completion: @escaping (_ data: SemesterInfo?) -> Void) {
        ApiService.shared.getSemesterInfo { (item) in
            completion(item)
        }
    }
    
    func getStudyGroups(completion: @escaping (_ data: [StudyGroup]) -> Void) {
        ApiService.shared.getStudyGroups { (groups) in
            completion(groups ?? [])
        }
    }
    
    func getSchedule(groupId: Int, completion: @escaping (_ data: [ScheduleItem]) -> Void) {
        ApiService.shared.getSchedule(groupId: groupId) { (schedule) in
            completion(schedule ?? [])
        }
    }
    
    // MARK: - Schedule Common Provider
    
    func getMietGroups(completion: @escaping (_ data: [String]) -> Void) {
        ApiService.shared.getMietGroups { (groups) in
            completion(groups ?? [])
        }
    }
    
    func getSchedule(groupName: String, completion: @escaping (_ data: ScheduleDataModel?) -> Void) {
        ApiService.shared.getSchedule(groupName: groupName) { (schedule) in
            completion(schedule)
        }
    }
 
    // MARK: - Semesters provider
    
    func getSemesters(completion: @escaping (SemestersHolder?) -> Void) {
        ApiService.shared.getSemesters { (semestersHolder) in
            completion(semestersHolder)
        }
    }
    
    func changeSemester(newSemester: Int, completion: @escaping (Bool) -> Void) {
        ApiService.shared.patchSemester(newSemester) { (updatedSemester) in
            completion(newSemester == updatedSemester?.semester)
        }
    }
    
    // MARK: - Roles provider
    
    func getRoles(completion: @escaping ([UserRoleItem]) -> Void) {
        ApiService.shared.getRoles { (roles) in
            completion(roles?.list ?? [])
        }
    }
    
    func changeRole(newRole: String, completion: @escaping (Bool) -> Void) {
        ApiService.shared.patchRole(newRole) { (updatedRole) in
            completion(newRole == updatedRole?.role)
        }
    }
    
    // MARK: - Teacher provider
    
    func loadTeacherProfile(completion: @escaping (ProfileItem?) -> Void) {
        ApiService.shared.getTeacherProfile { (profile) in
            completion(profile)
        }
    }
    
    func getTeacherDisciplines(completion: @escaping ([TeacherDisciplineHolder]) -> Void) {
        ApiService.shared.getTeacherDisciplines { (disciplines) in
            completion(disciplines ?? [])
        }
    }
    
    func getDisciplineStudents(_ disciplineId: Int, groupId: Int, completion: @escaping ([StudentStudyResultItem]) -> Void) {
        ApiService.shared.getDisciplneStudents(disciplineId, groupId: groupId) { (result) in
            completion(result ?? [])
        }
    }
    
    func getStudentStudyEvents(_ disciplineId: Int, groupId: Int, studentId: Int, completion: @escaping ([EventItem]) -> Void) {
        ApiService.shared.getStudentStudyEvents(disciplineId,
                                                groupId: groupId,
                                                studentId: studentId) { (result) in
            completion(result ?? [])
        }
    }
    
    func updateStudentEventScore(studentId: Int, disciplineId: Int, groupId: Int, eventId: Int, newScore: Double, completion:   @escaping (EventScoreItem?) -> Void) {
        ApiService.shared.updateStudentEventScore(studentId: studentId, disciplineId: disciplineId, groupId: groupId, eventId: eventId, newScore: newScore) { (result) in
            completion(result)
        }
    }
    
}
