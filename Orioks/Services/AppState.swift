//
//  AppState.swift
//  Orioks
//
//  Created by Генрих Берайлик on 27.11.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

enum AppState {
    case authorization, student, teacher, emptyRole
        
    static func current(_ completion: (AppState) -> ()) {
        guard let _: String? = Cache.fetch(.token) else {
            completion(.authorization)
            return
        }
        
        let userRole = UserRole.init(role: Cache.fetch(.userRole))
        switch userRole {
        case .student:
            completion(.student)
        case .teacher:
            completion(.teacher)
        default:
            completion(.emptyRole)
        }
    }
}
