//
//  PushService.swift
//  Orioks
//
//  Created by Генрих Берайлик on 04/04/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit
import UserNotifications

class PushService {
    
    static var pushToken: String? {
        get {
            return UserDefaults.standard.string(forKey: "PushToken")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "PushToken")
        }
    }
    
    static func requestPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            
            print("Permission granted: \(granted)")
            guard let pushToken = pushToken, granted else { return }
            registerPush(pushKey: pushToken)
        }
    }
    
    static func registerPush(pushKey: String) {
        guard let deviceUuid = UIDevice.current.identifierForVendor?.uuidString else { return }
        let pushData = PushDevice(deviceUuid: deviceUuid, pushKey: pushKey)
        ApiService.shared.registerPush(pushData: pushData) { (_) in }
    }
    
    static func unregisterPush() {
        guard let deviceUuid = UIDevice.current.identifierForVendor?.uuidString else { return }
        let pushData = PushDevice(deviceUuid: deviceUuid, pushKey: nil)
        ApiService.shared.unregisterPush(pushData: pushData)
    }
    
}
