//
//  ScheduleService.swift
//  Orioks
//
//  Created by Генрих Берайлик on 09.10.2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import Foundation

class ScheduleService {
 
    // MARK: - Properties
    
    private let dataLoader: DataLoader
    
    private var semesterStart: Date? {
        get {
            Cache.fetch(.semesterStart)
//            UserDefaults.standard.value(forKey: "SEMESTER_START") as? Date
        }
        set {
            Cache.put(value: newValue, for: .semesterStart)
            GroupCache.put(value: newValue, for: .semesterStart)
//            UserDefaults.standard.set(newValue, forKey: "SEMESTER_START")
        }
    }
    
    // MARK: - Initialization
    
    init() {
        dataLoader = DataLoader()
        
        loadSemesterInfo()
    }
    
    private func loadSemesterInfo() {
        dataLoader.getSemesterInfo { [weak self] (info) in
            guard let semesterStart = info?.semesterStart else { return }
            self?.semesterStart = self?.parseDate(dateStr: semesterStart)
        }
    }
    
    // MARK: - Helpers
    
    func getCurrentWeekDay() -> (week: WeekType, day: WeekDay) {
        guard let semStart = semesterStart else {
            return (.firstNumerator, .monday)
        }
        let currentDate = Date()
        
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: semStart)
        let date2 = calendar.startOfDay(for: currentDate)

        let components = calendar.dateComponents([.day], from: date1, to: date2)
        let weekCode = ((components.day ?? 0) / 7) % 4
        
        var weekDayCode = calendar.component(.weekday, from: currentDate)
        weekDayCode = (weekDayCode - 1) == 0 ? 7 : (weekDayCode - 1)
        
        let weekType = WeekType(rawValue: weekCode)
        let weekDay = WeekDay(rawValue: weekDayCode)
        
        return (weekType ?? .firstNumerator, weekDay ?? .monday)
    }
    
    private func parseDate(dateStr: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: dateStr)
    }
    
}
