//
//  String+Extensions.swift
//  Orioks
//
//  Created by Gena Beraylik on 02.10.2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    static func deFloat(number: Double) -> String {
        return "\(Int(number))"
    }
    
    private static func min(_ numbers: Int...) -> Int {
        return numbers.reduce(numbers[0], {$0 < $1 ? $0 : $1})
    }

    func hasQuerySubstring(_ query: String) -> Bool {
        for word in self.split(separator: " ") {
            if String.hasSubstring(text: String(word), query: query) {
                return true
            }
        }
        return false
    }
    
    private static func hasSubstring(text: String, query: String) -> Bool {
        var tempText = text.lowercased()
        var hasChar = true
        for qChar in query.lowercased() {
            hasChar = false
            for (j, tChar) in tempText.enumerated() {
                if tChar == qChar {
                    tempText.removeFirst(j+1)
                    hasChar = true
                    break
                }
            }
            if !hasChar { return false }
        }
        return hasChar
    }
    
}
