//
//  TableViewCell+Extensions.swift
//  Orioks
//
//  Created by Gena Beraylik on 14/10/2018.
//  Copyright © 2018 MIET. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    static var cellId: String {
        return self.description()
    }
}
