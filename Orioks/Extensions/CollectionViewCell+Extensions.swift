//
//  CollectionViewCell+Extensions.swift
//  Orioks
//
//  Created by Генрих Берайлик on 28/07/2019.
//  Copyright © 2019 MIET. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    static var cellId: String {
        return self.description()
    }
}
