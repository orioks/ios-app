//
//  UIView+Extensions.swift
//  Orioks
//
//  Created by Генрих Берайлик on 09.02.2020.
//  Copyright © 2020 MIET. All rights reserved.
//

import UIKit

extension UIView {
    
    func showLoader() {
        if viewWithTag(69) != nil {
            return
        }
        let loaderView = UIActivityIndicatorView()
        if #available(iOS 13.0, *) {
            loaderView.style = .large
        }
        loaderView.tag = 69
        loaderView.startAnimating()
        loaderView.layer.zPosition = 99
        self.addSubview(loaderView)
        loaderView.snp.makeConstraints { (make) in
//            make.size.equalTo(64)
            make.center.equalToSuperview()
        }
    }
    
    func hideLoader() {
        let loader = self.viewWithTag(69) as? UIActivityIndicatorView
        loader?.stopAnimating()
        loader?.removeFromSuperview()
    }
}
